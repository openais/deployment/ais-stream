pub mod input {
    use std::io::BufRead;
    use std::path::Path;

    use crate::{log::{log_error, log_info, log_warning}, utils::messages};


    #[derive(PartialEq, Eq)]
    enum FileType {
        List,
        Nmea,
        Rar,
    }


    pub struct Input {
        thread: Option<std::thread::JoinHandle<Result<(), std::io::Error>>>,
    }

    impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for Input {

        fn stop(&mut self) -> std::thread::Result<()> {
            match self.thread.take().map(std::thread::JoinHandle::join) {
                None => unreachable!(),
                Some(Err(e)) => Err(e),
                Some(Ok(Err(e))) => Err(Box::new(e)),
                Some(Ok(Ok(r))) => Ok(r),
            }
        }

    }


    pub fn spawn(path: &str, downstream: messages::Sender<String>) -> std::io::Result<Input>
    {
        let path = path.to_owned();
        let file_type = match file_type(&path) {
            None => {
                return Err(std::io::Error::new(std::io::ErrorKind::Other, "invalid input file type"));
            },
            Some(ft) => ft,
        };
        Ok(Input {
            thread: Some(std::thread::spawn(move || {
                match file_type {
                    FileType::List => run_list(path, &downstream),
                    FileType::Nmea => run_nmea(path, &downstream),
                    FileType::Rar => match run_rar(path, &downstream) {
                        Err(e) => return Err(std::io::Error::new(std::io::ErrorKind::Other, format!("unrar error: {e}"))),
                        Ok(r) => Ok(r),
                    },
                }?;
                downstream.close().unwrap_or_else(|e| {
                    log_error(format!("input file: error sending line downstream: {e}"));
                });
                Ok(())
            }))
        })
    }


    fn file_type<P>(path: P) -> Option<FileType>
    where
        P: AsRef<Path>,
    {
        let path: &Path = path.as_ref();
        match path.extension().and_then(std::ffi::OsStr::to_str) {
            Some("list") => Some(FileType::List),
            Some("nmea") => Some(FileType::Nmea),
            Some("rar") => Some(FileType::Rar),
            _ => None,
        }
    }


    fn run_list<P>(path: P, downstream: &messages::Sender<String>) -> std::io::Result<()>
    where
        P: AsRef<Path>,
    {
        log_info("file input: list");
        let file = std::fs::File::open(path)?;
        let b = std::io::BufReader::new(file);
        let mut count = 1;
        // for line in b.lines().map_while(Result::ok) {
        for line in b.lines() {
            match line {
                Err(e) => log_info(format!("file input list done: {}", e)),
                Ok(line) => {
                    if !line.starts_with("#") {
                        log_info(format!("file input list: processing file {:?}", line));
                        let path = line.trim();
                        match file_type(path) {
                            Some(FileType::Nmea) => { run_nmea(path, downstream).expect("run nmea ok"); },
                            Some(FileType::Rar) => { run_rar(path, downstream).expect("run unrar ok"); },
                            _ => { log_warning(format!("input list [line {}]: invalid file type", count)); }
                        }

                    }
                    count += 1;
                },
            }
        }
        Ok(())
    }


    fn run_nmea<P>(path: P, downstream: &messages::Sender<String>) -> std::io::Result<()>
    where
        P: AsRef<Path>,
    {
        log_info("file input: nmea");
        let file = std::fs::File::open(path)?;
        let b = std::io::BufReader::new(file);
        for line in b.lines().map_while(Result::ok) {
            downstream.send(line).unwrap_or_else(|e| {
                log_error(format!(
                    "input file: nmea: error sending line downstream: {e}"
                ));
            });
        }
        log_info("file input: done");
        Ok(())
    }


    fn run_rar<P>(path: P, downstream: &messages::Sender<String>) -> unrar::UnrarResult<()>
    where
        P: AsRef<Path>,
    {
        log_info("file input: unrar");
        let mut archive = unrar::Archive::new(&path).open_for_processing().unwrap(); // TODO Error handling.
        while let Ok(Some(header)) = archive.read_header() { // TODO Error handling.
            let archived_path = header.entry().filename.as_os_str();
            archive = if file_type(archived_path) == Some(FileType::Nmea) {
                // TODO Check on alternatives to stream the file contents.
                let (data, rest) = header.read()?;
                // drop(rest); // close the archive
                match String::from_utf8(data) { // TODO Vec<u8> instead of String.
                    Ok(content) => {
                        for line in content.lines() {
                            match downstream.send(line.to_owned()) {
                                Err(e) => {
                                        log_error(format!("input file: error sending line downstream: {e}"));
                                        return Err(unrar::error::UnrarError::from(unrar::error::Code::Unknown, unrar::error::When::Process))
                                },
                                Ok(_) => continue
                            }
                        }
                        rest
                    },
                    Err(_) => {
                        log_error("error: non-utf8 content");
                        return Err(unrar::error::UnrarError::from(unrar::error::Code::Unknown, unrar::error::When::Process))
                    },
                }
            } else {
                header.skip()?
            }
        }
        Ok(())
    }

}

pub mod output {
    use std::io::Write;

    use anyhow;

    use crate::{log::log_warning, utils::messages};

    pub struct Output {
        thread: Option<std::thread::JoinHandle<Result<(), std::io::Error>>>,
    }

    impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for Output {

        fn stop(&mut self) -> std::thread::Result<()> {
            match self.thread.take().map(std::thread::JoinHandle::join) {
                None => unreachable!(),
                Some(Err(e)) => Err(e),
                Some(Ok(Err(e))) => Err(Box::new(e)),
                Some(Ok(Ok(r))) => Ok(r),
            }
        }

    }

    pub fn spawn<T: Send + 'static, F>(
        out: Box<dyn Write + Send>,
        upstream: messages::SyncReceiver<T>,
        to_string: F,
    ) -> Output
    where
        F: Fn(T) -> anyhow::Result<String> + Send + 'static,
    {
        Output {
            thread: Some(std::thread::spawn(move || run(out, upstream, to_string))),
        }
    }

    #[tokio::main(flavor = "current_thread")]
    async fn run<T, F>(
        mut out: Box<dyn Write>,
        mut upstream: messages::SyncReceiver<T>,
        to_string: F,
    ) -> std::io::Result<()>
    where
        F: Fn(T) -> anyhow::Result<String>,
    {
        loop {
            match upstream.recv().await {
                None => break,
                Some(t) => match to_string(t) {
                    Ok(s) => {
                        let _ = out.write_all(s.as_bytes());
                    }
                    Err(e) => log_warning(format!("string conversion failed: {e:?}")),
                },
            }
        }
        Ok(())
    }
}
