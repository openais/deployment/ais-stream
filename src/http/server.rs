use std::{
    sync::{Arc, Mutex},
    time::{Duration, Instant},
};

use anyhow::anyhow;
use hyper::{server::conn::http1, service::service_fn};
use hyper_util::{rt::TokioIo, server::graceful::GracefulShutdown};
use prometheus_client::{metrics::gauge::Gauge, registry::Registry};
use tokio::{
    net::TcpListener,
    sync::{broadcast::error::RecvError, Notify},
    task,
};

use crate::http::{
    event::Event,
    handler::sse_service,
    stream::{SSESender, SSEStream},
};
use crate::log::{log_error, log_info, log_warning};
use crate::utils::messages;

pub struct SSEServer {
    metrics: Metrics,
    stop_token: Arc<Notify>,
    thread: Option<std::thread::JoinHandle<()>>,
}

impl SSEServer {
    pub fn spawn<T>(address: &str, upstream: messages::Messages<T>) -> Self
    where
        T: Send + Clone + TryInto<Vec<u8>> + 'static,
        T::Error: std::fmt::Debug,
    {
        let address = address.into();
        let metrics = Metrics::new();
        let thread_metrics = metrics.clone();
        let stop_token = Arc::new(Notify::new());
        let thread_stop_token = stop_token.clone();
        let thread = std::thread::spawn(move || {
            if let Err(e) = SSEServer::run(address, upstream, thread_metrics, thread_stop_token) {
                log_error(format!("SSE: error running HTTP server: {e:?}"));
            }
        });

        Self {
            metrics,
            stop_token,
            thread: Some(thread),
        }
    }

    pub fn register_metrics(&self, registry: &mut Registry) {
        self.metrics.register(registry);
    }

    #[tokio::main(flavor = "current_thread")]
    async fn run<T>(
        address: String,
        messages: messages::Messages<T>,
        metrics: Metrics,
        stop_token: Arc<Notify>,
    ) -> anyhow::Result<()>
    where
        T: Send + Clone + TryInto<Vec<u8>> + 'static,
        T::Error: std::fmt::Debug,
    {
        let listener = TcpListener::bind(address).await?;
        let http = http1::Builder::new();
        let graceful = GracefulShutdown::new();

        let message_receiver = messages
            .subscribe_async()
            .ok_or(anyhow!("unable to subscribe to messages channel"))?;
        let send_stop_token = stop_token.clone();
        let sse_stream = SSEStream::new();
        let sse_sender = sse_stream.sender();
        let sse_stream: Arc<Mutex<SSEStream>> = sse_stream.into();

        let send_task_handle = task::spawn(async move {
            Self::run_send(message_receiver, sse_sender, metrics, send_stop_token).await
        });

        loop {
            let sse_stream = sse_stream.clone();

            tokio::select! {
                biased;
                _ = stop_token.notified() => break,
                Ok((tcp_stream, client_addr)) = listener.accept() => {
                    let io = TokioIo::new(tcp_stream);
                    log_info(format!("SSE: client connected from ({:?})", client_addr));
                    let con = http.serve_connection(io, service_fn(move |req| { sse_service(sse_stream.clone(), req) }));

                    let graceful_con = graceful.watch(con);
                    task::spawn(async move {
                        if let Err(e) = graceful_con.await {
                            log_error(format!("SSE: error serving connection: {e:?}"));
                        }
                    });
                }
            }
        }

        send_task_handle.await?;

        Ok(())
    }

    async fn run_send<T>(
        mut message_receiver: messages::AsyncReceiver<T>,
        mut sse_sender: SSESender,
        metrics: Metrics,
        stop_token: Arc<Notify>,
    ) where
        T: Send + Clone + TryInto<Vec<u8>> + 'static,
        T::Error: std::fmt::Debug,
    {
        let mut last_client_count = Instant::now();
        loop {
            if last_client_count.elapsed() >= Duration::from_secs(5) {
                last_client_count = Instant::now();
                metrics.clients.set(sse_sender.connected_streams() as i64);
            }

            tokio::select! {
                biased;
                _ = stop_token.notified() => break,
                received = message_receiver.recv() => {
                    match received {
                        Err(RecvError::Lagged(n)) => {
                            log_warning(format!("SSE: lagging (skipped {n} messages)"));
                        }
                        Err(RecvError::Closed) => {
                            log_info("SSE: upstream channel closed!");
                            break;
                        }
                        Ok(line) => {
                            let data: Vec<u8> = match line.try_into() {
                                Ok(d) => d,
                                Err(e) => {
                                    log_warning(format!("SSE: unable to extract bytes: {e:?}"));
                                    continue;
                                }
                            };
                            let data = match String::from_utf8(data) {
                                Ok(d) => d,
                                Err(e) => {
                                    log_warning(format!("SSE: data contains invalid UTF-8: {e:?}"));
                                    continue;
                                }
                            };
                            let event = Event::new(data);
                            let _ = sse_sender.send(event);
                        }
                    }
                }
            }
        }
    }
}

impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for SSEServer {
    fn stop(&mut self) -> std::thread::Result<()> {
        self.stop_token.notify_waiters();
        match self.thread.take().map(std::thread::JoinHandle::join) {
            None => unreachable!(),
            Some(Err(e)) => Err(e),
            Some(Ok(())) => Ok(()),
        }
    }
}

#[derive(Clone, Default)]
struct Metrics {
    clients: Gauge,
}

impl Metrics {
    fn new() -> Self {
        Self::default()
    }

    fn register(&self, registry: &mut Registry) {
        registry.register(
            "sse_clients",
            "Number of connected SSE stream clients",
            self.clients.clone(),
        );
    }
}
