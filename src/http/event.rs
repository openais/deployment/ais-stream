use hyper::body::Bytes;

#[derive(Debug, Default)]
pub struct Event {
    data: String,
    event: Option<String>,
}

impl Event {
    pub fn new(data: String) -> Self {
        Self {
            data,
            ..Default::default()
        }
    }

    pub fn event(mut self, name: &str) -> Self {
        self.event = Some(String::from(name));

        self
    }
}

impl From<Event> for Bytes {
    fn from(value: Event) -> Self {
        let mut data = String::new();

        if let Some(event) = value.event {
            data.push_str(&format!("event: {event}\n"));
        }
        data.push_str(&format!("data: {}\n", value.data));
        data.push('\n');

        Bytes::from(data)
    }
}
