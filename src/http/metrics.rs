use bytes::Bytes;
use http_body_util::Full;
use hyper_util::rt::{TokioIo, TokioTimer};
use hyper::Response;
use hyper::server::conn::http1;
use hyper::service::service_fn;
use prometheus_client::registry::Registry;
use tokio::{select, sync::mpsc};
use std::{convert::Infallible, sync::Arc};
use tokio::net::TcpListener;

use crate::log::{log_error, log_info};


pub struct MetricsServer {
    thread: std::thread::JoinHandle<()>,
    stop_channel: mpsc::Sender<()>,
}


impl MetricsServer {

    pub fn spawn(address: &str, registry: Arc<Registry>) -> MetricsServer {
        let address = address.to_owned();
        let (stop, stop_rx) = mpsc::channel(1);
        let thread = std::thread::spawn(move || { MetricsServer::run(address, registry, stop_rx); } );
        MetricsServer {
            thread,
            stop_channel: stop,
        }
    }


    pub fn stop(self) {
        let _ = self.stop_channel.blocking_send(());
        let _ = self.thread.join();
    }


    #[tokio::main(flavor="current_thread")]
    async fn run(address: String, registry: Arc<Registry>, mut stop: mpsc::Receiver<()>) {
        match TcpListener::bind(&address).await {
            Err(e) => {
                log_error(format!("http_metrics_server: could not bind to {address}: {e}"));
                return
            }
            Ok(listener) => {
                log_info(format!("http_metrics_server: listening on http://{address}"));
                loop {
                    select! {
                        biased;

                        _ = stop.recv() => return,

                        Ok((tcp, _)) = listener.accept() => {
                            let io = TokioIo::new(tcp);
                            let registry = registry.clone();
                            tokio::task::spawn(async move {
                                if let Err(err) = http1::Builder::new()
                                    .timer(TokioTimer::new())
                                    .serve_connection(io, service_fn(|_| { MetricsServer::serve_metrics(registry.clone()) }))
                                    .await
                                {
                                    log_error(format!("http_metrics_server: error serving connection: {:?}", err));
                                }
                            });
                        }

                        else => {}
                    }
                }
            }
        }
    }


    async fn serve_metrics(registry: Arc<Registry>) -> Result<Response<Full<Bytes>>, Infallible> {
        let mut buffer = String::new();
        prometheus_client::encoding::text::encode(&mut buffer, &registry).unwrap();
        Ok(Response::new(Full::new(Bytes::from(buffer))))
    }

}
