use std::sync::{Arc, Mutex};

use anyhow::Result;
use hyper::body::Bytes;
use tokio::sync::broadcast::{channel, Sender};
use tokio_stream::wrappers::BroadcastStream;

use crate::http::event::Event;
use crate::log::log_info;

const SSE_STREAM_CAPACITY: usize = 500;

pub struct SSEStream {
    sender: Sender<Bytes>,
}

impl SSEStream {
    pub fn new() -> Self {
        let (sender, _) = channel(SSE_STREAM_CAPACITY);

        Self { sender }
    }

    pub fn sender(&self) -> SSESender {
        SSESender::new(self.sender.clone())
    }

    pub fn receive_stream(&self) -> BroadcastStream<Bytes> {
        log_info("SSE receive stream created!");
        BroadcastStream::new(self.sender.subscribe())
    }
}

impl Default for SSEStream {
    fn default() -> Self {
        Self::new()
    }
}

impl From<SSEStream> for Arc<Mutex<SSEStream>> {
    fn from(value: SSEStream) -> Self {
        Arc::new(Mutex::new(value))
    }
}

pub struct SSESender {
    sender: Sender<Bytes>,
}

impl SSESender {
    fn new(sender: Sender<Bytes>) -> Self {
        Self { sender }
    }

    pub fn send(&mut self, event: Event) -> Result<()> {
        Ok(self.sender.send(event.into()).map(|_| ())?)
    }

    pub fn connected_streams(&self) -> usize {
        self.sender.receiver_count()
    }
}
