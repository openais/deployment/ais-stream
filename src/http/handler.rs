use std::sync::{Arc, Mutex};

use anyhow::{bail, Result};
use futures_util::StreamExt;
use http_body_util::{BodyExt, Full, StreamBody};
use hyper::{
    body::{Bytes, Frame, Incoming},
    header, Method, Request, Response, StatusCode,
};

use crate::http::stream::SSEStream;

const PAGE_INDEX: &[u8] = b"<a href=\"stream\">stream: SSE JSON stream</a>";
const PAGE_NOT_FOUND: &[u8] = b"<h1>Not Found</h1>";

const ROUTE_INDEX: &str = "/";
const ROUTE_STREAM: &str = "/stream";

const MIME_EVENT_STREAM: &str = "text/event-stream";

type BoxBody = http_body_util::combinators::BoxBody<Bytes, anyhow::Error>;

pub async fn sse_service(
    stream: Arc<Mutex<SSEStream>>,
    req: Request<Incoming>,
) -> Result<Response<BoxBody>> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, ROUTE_INDEX) | (&Method::GET, "/index.html") => {
            Ok(Response::new(full(PAGE_INDEX)))
        }
        (&Method::GET, ROUTE_STREAM) => sse_stream_handler(stream).await,
        _ => Ok(Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(full(PAGE_NOT_FOUND))
            .unwrap()),
    }
}

fn full<T: Into<Bytes>>(chunk: T) -> BoxBody {
    Full::new(chunk.into()).map_err(|n| match n {}).boxed()
}

async fn sse_stream_handler(stream: Arc<Mutex<SSEStream>>) -> Result<Response<BoxBody>> {
    match stream.lock() {
        Ok(s) => {
            let stream = s
                .receive_stream()
                .map(|maybe_bytes| maybe_bytes.map(Frame::data).map_err(|e| e.into()));
            let body: BoxBody = BodyExt::boxed(StreamBody::new(stream));

            Ok(Response::builder()
                .header(header::CONTENT_TYPE, MIME_EVENT_STREAM)
                .header(header::ACCESS_CONTROL_ALLOW_ORIGIN, "*")
                .header(
                    header::ACCESS_CONTROL_ALLOW_HEADERS,
                    format!(
                        "{},{},{}",
                        header::ACCEPT,
                        header::AUTHORIZATION,
                        header::CONTENT_TYPE
                    ),
                )
                .header(header::ACCESS_CONTROL_ALLOW_METHODS, "GET")
                .body(body)?)
        }
        Err(e) => {
            bail!("Unable to get lock on SSE stream: {e:?}")
        }
    }
}
