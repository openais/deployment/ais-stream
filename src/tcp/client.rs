use prometheus_client::metrics::counter::Counter;
use prometheus_client::metrics::gauge::Gauge;
use prometheus_client::registry::Registry;
use socket2::{SockRef, TcpKeepalive};
use std::io::ErrorKind;
use std::sync::Arc;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use tokio::io::{AsyncBufReadExt, BufReader};
use tokio::net::TcpStream;
use tokio::select;
use tokio::sync::Notify;
use tokio::time::timeout;

use crate::log::{log_error, log_info, log_warning};
use crate::utils::messages;

const SOCKET_KEEP_ALIVE_S: u64 = 4;
const SOCKET_READ_TIMEOUT_S: u64 = 5;
const SOCKET_RECONNECT_TIMEOUT_S: u64 = 3;

pub struct TCPClient {
    metrics: TCPClientMetrics,
    thread: Option<std::thread::JoinHandle<Result<(), std::io::Error>>>,
    stop: Arc<Notify>,
}


impl TCPClient {

    pub fn spawn(address: &str, downstream: messages::Sender<String>) -> TCPClient {
        let address = address.to_owned();
        let metrics = TCPClientMetrics::new();
        let metrics_clone = metrics.clone();
        let stop = Arc::new(Notify::new());
        let thread_stop = stop.clone();
        let thread = std::thread::spawn(move || {
            TCPClient::run(address, downstream, metrics_clone, thread_stop);
            Ok(())
        });
        TCPClient {
            metrics,
            thread: Some(thread),
            stop,
        }
    }


    pub fn register_metrics(&self, registry: &mut Registry) {
        self.metrics.register(registry)
    }


    #[tokio::main(flavor="current_thread")]
    async fn run(address: String, downstream: messages::Sender<String>, metrics: TCPClientMetrics, stop: Arc<Notify>) {
        const TRIES: u8 = 5;
        let mut tries = TRIES;
        loop {
            select! {
                biased;

                _ = stop.notified() => return,

                conn = TcpStream::connect(&address) => {
                    match conn {
                        Err(e) => {
                            if e.kind() == ErrorKind::InvalidInput {
                                log_error(format!("tcp_client: {e}"));
                                break;
                            }
                            log_error(format!("tcp_client: {e}"));
                            if tries == 0 {
                                log_error("tcp_client: exhausted number of tries");
                                break;
                            } else {
                                log_info("tcp_client: trying to reconnect...");
                                tries -= 1;
                                tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
                            }
                        }
                        Ok(stream) => {
                            let sock = SockRef::from(&stream);
                            let keep_alive = TcpKeepalive::new()
                                .with_time(Duration::from_secs(SOCKET_KEEP_ALIVE_S))
                                .with_interval(Duration::from_secs(SOCKET_KEEP_ALIVE_S));
                            if let Err(e) = sock.set_tcp_keepalive(&keep_alive) {
                                log_warning(format!("tcp_client: unable to set keep alive: {e:?}. Will continue without!"));
                            }

                            let ts: u64 = SystemTime::now().duration_since(UNIX_EPOCH).unwrap().as_secs();
                            metrics.connection_timestamp.set(ts.try_into().unwrap());
                            metrics.connections.inc();
                            log_info(format!("tcp_client: connected to {address}"));
                            tries = TRIES;
                            let mut buffered = BufReader::new(stream);
                            let mut line = String::new();
                            loop {
                                select!{
                                    biased;

                                    _ = stop.notified() => return,

                                    timed_read_result = timeout(Duration::from_secs(SOCKET_READ_TIMEOUT_S), buffered.read_line(&mut line)) => {
                                        match timed_read_result {
                                            Ok(read_result) => match read_result {
                                                Err(e) => {
                                                    log_error(format!("ERROR: {e}"));
                                                    break;
                                                }
                                                Ok(0) => {
                                                    log_info("tcp_client: upstream disconnected");
                                                    break;
                                                }
                                                Ok(n) => {
                                                    metrics.bytes.inc_by(n.try_into().unwrap());
                                                    metrics.lines.inc();
                                                    let _ = downstream.async_send(line.clone()).await;
                                                    line.clear();
                                                }
                                            }
                                            Err(_) => {
                                                log_error(format!("tcp_client: Unable to read data for {} secs. Will retry!", SOCKET_READ_TIMEOUT_S));
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            // Take a nap before retrying to connect, gives the AIS feed time to
                            // realise we've closed on our end
                            tokio::time::sleep(Duration::from_secs(SOCKET_RECONNECT_TIMEOUT_S)).await;
                        }
                    }
                }
            }
        }
    }

}


impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for TCPClient {

    fn stop(&mut self) -> std::thread::Result<()> {
        self.stop.notify_one();
        match self.thread.take().map(std::thread::JoinHandle::join) {
            None => unreachable!(),
            Some(Err(e)) => Err(e),
            Some(Ok(Err(e))) => Err(Box::new(e)),
            Some(Ok(Ok(r))) => Ok(r),
        }
    }

}


struct TCPClientMetrics {
    connection_timestamp: Gauge,
    connections: Counter,
    bytes: Counter,
    lines: Counter,
}


impl TCPClientMetrics {

    fn new() -> TCPClientMetrics {
        TCPClientMetrics {
            connection_timestamp: Gauge::default(),
            connections: Counter::default(),
            bytes: Counter::default(),
            lines: Counter::default(),
        }
    }


    fn clone(&self) -> TCPClientMetrics {
        TCPClientMetrics {
            connection_timestamp: self.connection_timestamp.clone(),
            connections: self.connections.clone(),
            bytes: self.bytes.clone(),
            lines: self.lines.clone(),
        }
    }


    fn register(&self, registry: &mut Registry) {
        registry.register(
            "connection_timestamp",
            "Last connection time as the number of seconds since the Unix epoch",
            self.connection_timestamp.clone(),
        );
        registry.register(
            "connections",
            "Number of times a connection to the server was established",
            self.connections.clone(),
        );
        registry.register(
            "bytes",
            "Number of transferred bytes",
            self.bytes.clone(),
        );
        registry.register(
            "lines",
            "Number of transferred lines",
            self.lines.clone(),
        );
    }

}
