use prometheus_client::{metrics::{counter::Counter, gauge::Gauge}, registry::Registry};
use std::sync::Arc;
use tokio::io::AsyncWriteExt;
use tokio::select;
use tokio::sync::{broadcast, Notify};

use crate::{
    log::{log_error, log_info, log_warning},
    utils::messages,
};

/// TCP server that broadcasts messages from an upstream server.
pub struct TCPServer {
    metrics: TCPServerMetrics,
    thread: Option<std::thread::JoinHandle<()>>,
    stop: Arc<Notify>,
}


impl TCPServer {
    pub fn spawn<T>(
        address: &str,
        upstream: messages::Messages<T>,
    ) -> TCPServer
    where
        T: Send + Clone + TryInto<Vec<u8>> + 'static,
        T::Error: std::fmt::Debug,
    {
        let address = address.to_owned();
        let metrics = TCPServerMetrics::new();
        let metrics_clone = metrics.clone();
        let stop = Arc::new(Notify::new());
        let thread_stop = stop.clone();
        let thread = std::thread::spawn(move || {
            TCPServer::run(address, upstream, metrics_clone, thread_stop);
        });
        TCPServer {
            metrics,
            thread: Some(thread),
            stop,
        }
    }


    pub fn register_metrics(&self, registry: &mut Registry) {
        self.metrics.register(registry)
    }

    #[tokio::main(flavor = "current_thread")]
    async fn run<T>(
        address: String,
        messages: messages::Messages<T>,
        metrics: TCPServerMetrics,
        stop: Arc<Notify>,
    )
    where
        T: Send + Clone + TryInto<Vec<u8>> + 'static,
        T::Error: std::fmt::Debug,
    {
        let listener = tokio::net::TcpListener::bind(&address).await.unwrap();
        log_info(format!("tcp_server: listening on {address}"));

        loop {
            // metrics.upstream_buffer.set(messages.len().try_into().unwrap()); // TODO
            select! {
                biased;

                _ = stop.notified() => return,

                from_client = listener.accept() => {
                    match from_client {
                        Err(e) => {
                            log_error(format!("tcp_server: error while accepting connection: {e}"));
                        }
                        Ok(conn) => {
                            match messages.subscribe_async() {
                                None => {
                                    log_error("tcp_server: error while accepting connection: message channel not available");
                                },
                                Some(sender) => { tokio::spawn(Self::tcp_server_connection(conn, stop.clone(), sender, metrics.clone())); },
                            }
                        }
                    }
                }

            }
        }
    }


    /// TCP server connection handler.
    async fn tcp_server_connection<T>(
        (mut socket, addr): (tokio::net::TcpStream, core::net::SocketAddr),
        stop: Arc<Notify>,
        mut rx: messages::AsyncReceiver<T>,
        metrics: TCPServerMetrics,
    )
    where
        T: Send + Clone + TryInto<Vec<u8>> + 'static,
        T::Error: std::fmt::Debug,
    {
        metrics.clients.inc();
        log_info(format!("tcp_server: new connection: {addr}"));
        let (_conn_rx, mut conn_tx) = socket.split();
        // drop(conn_rx); // no need to read from the client
        loop {
            match rx.recv().await {
                Err(broadcast::error::RecvError::Lagged(n)) => {
                    log_warning(format!("tcp_server: lagging (skipped {n} messages)"));
                }
                Err(broadcast::error::RecvError::Closed) => {
                    log_info("tcp_server: upstream closed");
                    stop.notify_one();
                    break;
                }
                Ok(line) => {
                    let data: Vec<u8> = match line.try_into() {
                        Ok(mut data) => {
                            // If line doesn't end with new line '\n', add it
                            if let Some(b) = data.last() {
                                if *b != 0x0a {
                                    data.extend_from_slice(b"\n");
                                }
                            }
                            data
                        }
                        Err(e) => {
                            log_warning(format!("tcp_server: unable to extract bytes {e:?}, skipping!"));
                            continue;
                        }
                    };
                    if conn_tx.write_all(&data).await.is_err() {
                        metrics.clients.dec();
                        log_info(format!("tcp_server: client {addr} disconnected"));
                        break;
                    }
                    if conn_tx.flush().await.is_err() {
                        metrics.clients.dec();
                        log_info(format!("tcp_server: client {addr} disconnected"));
                        break;
                    }
                }
            }
        }
    }

}


impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for TCPServer {

    fn stop(&mut self) -> std::thread::Result<()> {
        self.stop.notify_one();
        match self.thread.take().map(std::thread::JoinHandle::join) {
            None => unreachable!(),
            Some(Err(e)) => Err(e),
            Some(Ok(())) => Ok(()),
        }
    }

}


#[derive(Clone)]
pub struct TCPServerMetrics {
    clients: Gauge,
    upstream_buffer: Gauge,
    lines: Counter,
}


impl TCPServerMetrics {

    pub fn new() -> TCPServerMetrics {
        TCPServerMetrics {
            clients: Gauge::default(),
            upstream_buffer: Gauge::default(),
            lines: Counter::default(),
        }
    }


    fn register(&self, registry: &mut Registry) {
        registry.register(
            "clients",
            "Number of connected clients",
            self.clients.clone(),
        );
        registry.register(
            "upstream_buffer",
            "Number of items in the upstream buffer",
            self.upstream_buffer.clone(),
        );
        registry.register(
            "lines",
            "Number of transferred lines",
            self.lines.clone(),
        );
    }

}


impl Default for TCPServerMetrics {

    fn default() -> Self {
        Self::new()
    }

}
