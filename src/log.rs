#[doc(hidden)]
#[derive(Debug)]
#[derive(PartialEq)]
pub enum LogLevel {
    ERROR,
    WARNING,
    INFO,
}


/// Print a log message.
fn log<S: AsRef<str>>(level: LogLevel, message: S) {
    let ts = chrono::Utc::now();
    println!("{} {:?} {}", ts.format("%FT%T%.3fZ"), level, message.as_ref());
}


/// Print an ERROR log message.
#[doc(hidden)]
pub fn log_error<S: AsRef<str>>(message: S) {
    log(LogLevel::ERROR, message);
}


/// Print an WARNING log message.
#[doc(hidden)]
pub fn log_warning<S: AsRef<str>>(message: S) {
    log(LogLevel::WARNING, message);
}


/// Print an INFO log message.
#[doc(hidden)]
pub fn log_info<S: AsRef<str>>(message: S) {
    log(LogLevel::INFO, message);
}
