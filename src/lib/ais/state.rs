use std::collections::HashMap;

use chrono::DurationRound;
use geo::Intersects;
use geos::Geometry;
use serde::Serialize;

use super::types::MMSI;

use crate::utils::serialisation;

pub struct State {
    pub region: Option<Geometry>,
    pub ship_infos: HashMap<MMSI, ShipInfo>,
    pub ship_voyages: HashMap<MMSI, ShipVoyage>,
    pub ship_states: HashMap<MMSI, ShipState>,
    pub ships: HashMap<MMSI, Ship>,
    pub previous_time_bucket: Option<(
        chrono::DateTime<chrono::Utc>,
        HashMap<MMSI, ShipGridCellInfo>,
    )>,
    pub current_time_bucket: (
        chrono::DateTime<chrono::Utc>,
        HashMap<MMSI, ShipGridCellInfo>,
    ),
}

pub fn new(timestamp: chrono::DateTime<chrono::Utc>) -> State {
    State {
        region: None,
        ship_infos: HashMap::new(),
        ship_voyages: HashMap::new(),
        ship_states: HashMap::new(),
        ships: HashMap::new(),
        previous_time_bucket: None,
        current_time_bucket: (timestamp, HashMap::new()),
    }
}

const EXPIRATION_DELAY: u64 = 300_000;


fn find_grid_cell(grid: &rstar::RTree<crate::lib::geo::GridCell>, lon: i32, lat: i32) -> Option<&crate::lib::geo::GridCell> {
    let lon = lon as f64 / 600_000.0;
    let lat = lat as f64 / 600_000.0;
    let point: geo_types::geometry::Point = (lon, lat).into();
    let envelope = rstar::AABB::<geo_types::Point>::from_corners(point, point);
    let mut elems = grid.locate_in_envelope_intersecting(&envelope);
    elems.next()
}

impl State {
    fn time_bucket(timestamp: chrono::DateTime<chrono::Utc>) -> chrono::DateTime<chrono::Utc> {
        timestamp
            .duration_trunc(chrono::TimeDelta::try_days(1).unwrap())
            .unwrap()
    }

    fn time_bucket_expired(timestamp: chrono::DateTime<chrono::Utc>, current_timestamp: chrono::DateTime<chrono::Utc>) -> bool {
        timestamp < (current_timestamp - chrono::Days::new(1) + std::time::Duration::from_millis(EXPIRATION_DELAY))
    }

    fn time_bucket_update(&self, timestamp: chrono::DateTime<chrono::Utc>, values: HashMap<MMSI, ShipGridCellInfo>) -> (chrono::DateTime<chrono::Utc>, Vec<GridCellInfo3>) {
        let mut hm = HashMap::new();
        for (mmsi, grid_cell_info) in values.iter() {
            let shiptype = self.ships.get(mmsi).and_then(|s| s.shiptype);
            for cell in &grid_cell_info.cells {
                // TODO time-bucket total time.
                hm.entry((cell.grid, shiptype))
                    .and_modify(|a: &mut GridCellInfo3| {
                        a.count += cell.count;
                        a.duration += cell.duration;
                    })
                    .or_insert(GridCellInfo3 {
                        grid: cell.grid,
                        shiptype,
                        count: cell.count,
                        duration: cell.duration,
                    });
            }
        }
        (timestamp, hm.into_values().collect())
    }

    fn update_time_bucket(
        &mut self, grid: &rstar::RTree<crate::lib::geo::GridCell>,
        lines: &Option<Vec<crate::lib::geo::Line>>,
        mmsi: MMSI,
        timestamp: chrono::DateTime<chrono::Utc>,
        lon: i32,
        lat: i32
    ) -> Vec<i32> {
        let current_grid = find_grid_cell(grid, lon, lat);
        let flon = lon as f64 / 600_000.0;
        let flat = lat as f64 / 600_000.0;
        let coord: geo_types::Coord = (flon, flat).into();
        let mut intersected_lines: Vec<i32> = Vec::new();
        self.current_time_bucket.1.entry(mmsi)
            .and_modify(|a| {
                let previous_coord = std::mem::replace(&mut a.coord, coord);
                let last = a.cells.last_mut().unwrap();
                let delta = timestamp - a.timestamp;
                // grid
                if last.grid == current_grid.map(|a| a.data) {
                    last.count += 1;
                    last.duration += delta;
                } else {
                    // TODO Split the time based on the length of the line
                    // intersection with each grid.
                    let half_delta = delta / 2;
                    last.duration += half_delta;
                    a.cells.push(GridCellInfo2 {
                        grid: current_grid.map(|a| a.data),
                        count: 1,
                        duration: half_delta,
                    });
                }
                // lines
                let ship_line = geo_types::Line::new(previous_coord, coord);
                lines.as_ref().map(|lines| {
                    for line in lines {
                        if line.1.intersects(&ship_line) {
                            intersected_lines.push(line.0);
                        }
                    }
                });
            })
            .or_insert_with(|| {
                let mut delta = chrono::TimeDelta::default();
                let _ = self.previous_time_bucket.as_mut().map(|p| {
                    p.1.entry(mmsi).and_modify(|a| {
                        // TODO Split the time based on the length of the line
                        // intersection with each grid.
                        delta = (timestamp - a.timestamp) / 2;
                        a.cells.last_mut().unwrap().duration += delta;
                    })
                });
                let cell = GridCellInfo2 {
                    grid: current_grid.map(|a| a.data),
                    count: 1,
                    duration: delta,
                };
                ShipGridCellInfo {
                    timestamp,
                    coord,
                    cells: vec![cell],
                }
            });
        intersected_lines
    }


    pub fn ship<T>(&mut self, setter: fn(&mut Ship, &T) -> bool, mmsi: MMSI, value: &T) -> Operation
    {
        let mut new = true;
        let mut updated = false;
        self.ships.entry(mmsi)
            .and_modify(|s| {
                new = false;
                updated = setter(s, value);
            })
            .or_insert({
                let mut ship = Ship::default();
                let _ = setter(&mut ship, value);
                ship
            });
        if new {
            Operation::Add
        } else if updated {
            Operation::Update
        } else {
            Operation::NoOp
        }
    }

    pub fn ship_position(
        &mut self,
        grid: &Option<rstar::RTree<crate::lib::geo::GridCell>>,
        lines: &Option<Vec<crate::lib::geo::Line>>,
        mmsi: MMSI,
        value: &ShipState
    ) -> (Operation, chrono::DateTime<chrono::Utc>, Option<(chrono::DateTime<chrono::Utc>, Vec<GridCellInfo3>)>, (Option<u8>, Vec<i32>))
    {
        let mut new = true;
        let mut updated = false;
        let mut intersected_lines = Vec::new();
        let entry = self.ships.entry(mmsi)
            .and_modify(|s| {
                new = false;
                updated = Ship::set_position(s, value);
            })
            .or_insert({
                let mut ship = Ship::default();
                let _ = Ship::set_position(&mut ship, value);
                ship
            });
        let mut ts_bucket_update = None;
        let ts_bucket = Self::time_bucket(value.timestamp);
        let shiptype = entry.shiptype;
        grid.as_ref().map(|grid| {
            if self.current_time_bucket.0 != ts_bucket {
                if let Some(previous_ts_bucket) = self.previous_time_bucket.take() {
                    ts_bucket_update = Some(self.time_bucket_update(previous_ts_bucket.0, previous_ts_bucket.1));
                }
                self.previous_time_bucket = Some(std::mem::replace(&mut self.current_time_bucket, (ts_bucket, HashMap::new())));
            } else if self.previous_time_bucket.as_ref().is_some_and(|previous| Self::time_bucket_expired(previous.0, value.timestamp)) {
                let (bucket, values) = self.previous_time_bucket.take().unwrap();
                ts_bucket_update = Some(self.time_bucket_update(bucket, values));
            }
            intersected_lines = self.update_time_bucket(grid, lines, mmsi, value.timestamp, value.lon, value.lat);
        });
        if new {
            (Operation::Add, ts_bucket, ts_bucket_update, (shiptype, intersected_lines))
        } else if updated {
            (Operation::Update, ts_bucket, ts_bucket_update, (shiptype, intersected_lines))
        } else {
            (Operation::NoOp, ts_bucket, ts_bucket_update, (shiptype, intersected_lines))
        }
    }
}

trait Zeroable {
    fn is_zero(&self) -> bool;
}

impl Zeroable for u8 {
    #[inline]
    fn is_zero(&self) -> bool {
        *self == 0
    }
}

impl Zeroable for u16 {
    #[inline]
    fn is_zero(&self) -> bool {
        *self == 0
    }
}

pub enum Operation {
    Add,
    Update,
    NoOp,
}

#[derive(Clone, PartialEq, Eq)]
pub struct Ship {
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub ts_update: chrono::DateTime<chrono::Utc>,
    pub ais_version: Option<u8>,  // AIS version level // TODO enum
    pub imo: Option<u32>,         // IMO identification
    pub callsign: Option<String>, // callsign
    pub shipname: String,         // vessel name
    pub shiptype: Option<u8>,     // ship type code
    pub to_bow: u16,
    pub to_stern: u16,
    pub to_port: u8,
    pub to_starboard: u8,
    pub epfd: u8, // type of position fix device // TODO enum // Electronic Position Fixing Device (EPFD)
    pub draught: u8, // decimeters
    pub destination: String, // ship destination
    pub month: u8, // UTC month
    pub day: u8,  // UTC day
    pub hour: u8, // UTC hour
    pub minute: u8, // UTC minute
    pub status: Option<u8>, // navigation status // TODO enum
    pub turn: Option<u8>, // rate of turn
    pub speed: Option<u16>, // speed over ground in deciknots
    pub accuracy: bool, // position accuracy // TODO enum?
    pub lon: i32, // longitude
    pub lat: i32, // latitude
    pub course: Option<u16>, // course over ground
    pub heading: Option<u16>, // true heading
    pub second: u8, // UTC second when the report was generated by the electronic position system (EPFS)
    pub maneuver: Option<u8>, // maneuver indicator // TODO enum
    pub raim: bool, // Receiver Autonomous Integrity Monitoring (RAIM) flag of electronic position fixing device // TODO enum
    pub radio: Option<u32>, // radio status bits
}

impl Default for Ship {
    fn default() -> Self {
        Ship {
            timestamp: chrono::DateTime::default(), // TODO validate default
            ts_update: chrono::DateTime::default(), // TODO validate default
            ais_version: None,                      // TODO validate default
            imo: None,                              // TODO validate default
            callsign: None,                         // TODO validate default
            shipname: String::default(),            // TODO validate default
            shiptype: None,                         // TODO validate default
            to_bow: 0,                              // TODO validate default
            to_stern: 0,                            // TODO validate default
            to_port: 0,                             // TODO validate default
            to_starboard: 0,                        // TODO validate default
            epfd: 0,                                // TODO validate default
            draught: 0,                             // TODO validate default
            destination: String::default(),         // TODO validate default
            month: 0,                               // TODO validate default
            day: 0,                                 // TODO validate default
            hour: 0,                                // TODO validate default
            minute: 0,                              // TODO validate default
            status: None,                           // TODO validate default
            turn: None,                             // TODO validate default
            speed: None,                            // TODO validate default
            accuracy: false,                        // TODO validate default
            lon: 181,
            lat: 91,
            course: None,   // TODO validate default
            heading: None,  // TODO validate default
            second: 0,      // TODO validate default
            maneuver: None, // TODO validate default
            raim: false,    // TODO validate default
            radio: None,    // TODO validate default
        }
    }
}

impl Ship {
    pub fn set_info(entry: &mut Ship, info: &ShipInfo) -> bool {
        let mut updated = false;
        entry.ts_update = info.timestamp;
        updated = update_if_some(&mut entry.ais_version, info.ais_version) || updated;
        updated = update_if_some(&mut entry.imo, info.imo) || updated;
        updated = update_if_some(&mut entry.callsign, info.callsign.clone()) || updated;
        updated = update_if_different(&mut entry.shipname, info.shipname.clone()) || updated;
        updated = update_if_some(&mut entry.shiptype, info.shiptype) || updated;
        updated = update_if_not_zero(&mut entry.to_bow, info.to_bow) || updated;
        updated = update_if_not_zero(&mut entry.to_stern, info.to_stern) || updated;
        updated = update_if_not_zero(&mut entry.to_port, info.to_port) || updated;
        updated = update_if_not_zero(&mut entry.to_starboard, info.to_starboard) || updated;
        updated = update_if_different(&mut entry.epfd, info.epfd) || updated;
        if entry.epfd == 0 || (info.epfd != 0 && info.epfd != 15) {
            updated = update_if_not_zero(&mut entry.to_bow, info.to_bow) || updated;
            updated = update_if_not_zero(&mut entry.to_stern, info.to_stern) || updated;
            updated = update_if_not_zero(&mut entry.to_port, info.to_port) || updated;
            updated = update_if_not_zero(&mut entry.to_starboard, info.to_starboard) || updated;
        }
        if info.epfd != entry.epfd && (entry.epfd == 0 || info.epfd != 15) {
            entry.epfd = info.epfd; updated = true;
        }
        updated
    }

    pub fn set_voyage(entry: &mut Ship, voyage: &ShipVoyage) -> bool {
        let mut updated = false;
        entry.ts_update = voyage.timestamp;
        updated = update_if_different(&mut entry.draught, voyage.draught) || updated;
        updated = update_if_different(&mut entry.destination, voyage.destination.clone()) || updated;
        updated = update_if_different(&mut entry.month, voyage.month) || updated;
        updated = update_if_different(&mut entry.day, voyage.day) || updated;
        updated = update_if_different(&mut entry.hour, voyage.hour) || updated;
        updated = update_if_different(&mut entry.minute, voyage.minute) || updated;
        updated
    }

    pub fn set_position(entry: &mut Ship, state: &ShipState) -> bool {
        let mut updated = false;
        entry.ts_update = state.timestamp;
        updated = update_if_some(&mut entry.status, state.status) || updated;
        updated = update_if_some(&mut entry.turn, state.turn) || updated;
        updated = update_if_some(&mut entry.speed, state.speed) || updated;
        updated = update_if_different(&mut entry.accuracy, state.accuracy) || updated;
        updated = update_if_different(&mut entry.lon, state.lon) || updated;
        updated = update_if_different(&mut entry.lat, state.lat) || updated;
        updated = update_if_some(&mut entry.course, state.course) || updated;
        updated = update_if_some(&mut entry.heading, state.heading) || updated;
        updated = update_if_different(&mut entry.second, state.second) || updated;
        updated = update_if_some(&mut entry.maneuver, state.maneuver) || updated;
        updated = update_if_different(&mut entry.raim, state.raim) || updated;
        updated = update_if_some(&mut entry.radio, state.radio) || updated;
        updated
    }
}

#[derive(Clone, PartialEq, Eq, Serialize)]
pub struct ShipInfo {
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub ts_update: chrono::DateTime<chrono::Utc>,
    pub ais_version: Option<u8>,  // AIS version level // TODO enum
    pub imo: Option<u32>,         // IMO identification
    pub callsign: Option<String>, // callsign
    pub shipname: String,         // vessel name
    pub shiptype: Option<u8>,     // ship type code
    pub to_bow: u16,
    pub to_stern: u16,
    pub to_port: u8,
    pub to_starboard: u8,
    pub epfd: u8, // type of position fix device // TODO enum // Electronic Position Fixing Device (EPFD)
}

#[derive(Clone, PartialEq, Eq, Serialize)]
pub struct ShipVoyage {
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub draught: u8,         // decimeters
    pub destination: String, // ship destination
    pub month: u8,           // UTC month
    pub day: u8,             // UTC day
    pub hour: u8,            // UTC hour
    pub minute: u8,          // UTC minute
}

#[derive(Clone, PartialEq, Eq, Serialize)]
pub struct ShipState {
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub status: Option<u8>, // navigation status // TODO enum
    pub turn: Option<u8>,   // rate of turn
    #[serde(with = "serialisation::speed::maybe_deciknots")]
    pub speed: Option<u16>, // speed over ground in deciknots
    pub accuracy: bool,     // position accuracy // TODO enum?
    #[serde(with = "serialisation::position::lon_deg")]
    pub lon: i32, // longitude
    #[serde(with = "serialisation::position::lat_deg")]
    pub lat: i32, // latitude
    #[serde(with = "serialisation::course::maybe_tenth_of_deg")]
    pub course: Option<u16>, // course over ground
    #[serde(with = "serialisation::maybe_heading")]
    pub heading: Option<u16>, // true heading
    pub second: u8, // UTC second when the report was generated by the electronic position system (EPFS)
    pub maneuver: Option<u8>, // maneuver indicator // TODO enum
    pub raim: bool, // Receiver Autonomous Integrity Monitoring (RAIM) flag of electronic position fixing device // TODO enum
    pub radio: Option<u32>, // radio status bits
}

#[derive(Clone, Serialize)]
pub struct GridPosition {
    pub grid: i32,
    pub mmsi: MMSI,
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub shiptype: Option<u8>,
    pub width: Option<u16>,
    pub length: Option<u16>,
    pub draught: Option<u8>,
}

fn update_if_some<T>(prop: &mut Option<T>, value: Option<T>) -> bool
where
    T: PartialEq,
{
    if value.is_some() && *prop != value {
        *prop = value;
        true
    } else {
        false
    }
}

fn update_if_not_zero<T>(prop: &mut T, value: T) -> bool
where
    T: PartialEq + Zeroable,
{
    if !value.is_zero() && *prop != value {
        *prop = value;
        true
    } else {
        false
    }
}

fn update_if_different<T>(prop: &mut T, value: T) -> bool
where
    T: PartialEq,
{
    if *prop != value {
        *prop = value;
        true
    } else {
        false
    }
}

pub struct ShipGridCellInfo {
    pub timestamp: chrono::DateTime<chrono::Utc>,
    pub coord: geo_types::Coord,
    pub cells: Vec<GridCellInfo2>,
}

pub struct GridCellInfo2 {
    pub grid: Option<i32>,
    pub count: usize,
    pub duration: chrono::TimeDelta,
}

#[derive(Clone, Serialize)]
pub struct GridCellInfo3 {
    pub grid: Option<i32>,
    pub shiptype: Option<u8>,
    pub count: usize,
    #[serde(with = "serialisation::delta::milliseconds")]
    pub duration: chrono::TimeDelta,
}
