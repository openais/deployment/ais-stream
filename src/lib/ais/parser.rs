use nom::branch;
use nom::bytes::complete::{tag, take, take_till, take_until, take_until1};
use nom::character::complete::digit0;
use nom::combinator::{opt, value};
use nom::multi::separated_list0;
use nom::IResult;


#[derive(PartialEq, Debug)]
pub struct Packet<'a> {
    pub tag: Option<PacketTag<'a>>,
    pub nmea: PacketNMEA<'a>,
}


#[derive(PartialEq, Debug)]
pub struct PacketTag<'a> {
    pub fields: Vec<PacketTagField<'a>>,
    pub checksum: &'a [u8], // TODO better type
}


#[derive(PartialEq, Debug)]
pub struct PacketNMEA<'a> {
    pub nmea_type: &'a [u8],
    pub fragment_count: &'a [u8],
    pub fragment_number: &'a [u8],
    pub sequence_id: &'a [u8],
    pub channel: NMEAChannel,
    pub payload: &'a [u8],
    pub fill: u8,
    pub checksum: &'a [u8], // TODO mandatory and better type
}


#[derive(Clone, Copy, PartialEq, Debug)]
pub enum NMEAChannel {
    None=0,
    A=1,
    B=2,
    C=3,
}


pub fn parse(input: &[u8]) -> Result<Packet, String> {
    match parse_packet(input) {
        Err(e) => Err(e.to_string()), // TODO improve error reporting
        Ok((_, r)) => Ok(r),
    }
}


pub fn parse_packet(input: &[u8]) -> IResult<&[u8], Packet> {
    let i = input;
    let (i, tag) = opt(parse_tag)(i)?;
    let (i, nmea) = parse_nmea(i)?;
    Ok((i, Packet {
        tag,
        nmea,
    }))
}


#[derive(PartialEq, Debug)]
pub struct PacketTagField<'a> {
    name: &'a [u8],
    value: &'a [u8],
}


impl PacketTagField<'_> {

    pub fn as_tuple(&self) -> (Vec<u8>, Vec<u8>) {
        (Vec::from(self.name), Vec::from(self.value))
    }

}


pub fn parse_tag(i: &[u8]) -> IResult<&[u8], PacketTag> {
    let (i, _) = tag(b"\\")(i)?;
    let (i, fields) = separated_list0(tag(b","), parse_tag_field)(i)?;
    let (i, _) = tag(b"*")(i)?;
    let (i, checksum) = take(2u8)(i)?;
    let (i, _) = tag(b"\\")(i)?;
    Ok((i, PacketTag{ fields, checksum }))
}


fn parse_tag_field(i: &[u8]) -> IResult<&[u8], PacketTagField> {
    let (i, name) = take_until(b":" as &[u8])(i)?;
    let (i, _) = tag(b":")(i)?;
    let (i, value) = take_till(|c| c == b'*' || c == b'\\' || c == b',')(i)?;
    Ok((i, PacketTagField { name, value }))
}


pub fn parse_nmea(i: &[u8]) -> IResult<&[u8], PacketNMEA> {
    let (i, nmea_type) = take_until1(b"," as &[u8])(i)?;
    let (i, _) = tag(b",")(i)?;
    let (i, fragment_count) = digit0(i)?;
    let (i, _) = tag(b",")(i)?;
    let (i, fragment_number) = digit0(i)?;
    let (i, _) = tag(b",")(i)?;
    let (i, sequence_id) = take_until(b"," as &[u8])(i)?;
    let (i, _) = tag(b",")(i)?;
    let (i, channel) = branch::alt((
        value(NMEAChannel::A, tag(b"A")),
        value(NMEAChannel::A, tag(b"1")),
        value(NMEAChannel::B, tag(b"B")),
        value(NMEAChannel::B, tag(b"2")),
        value(NMEAChannel::C, tag(b"C")),
        value(NMEAChannel::C, tag(b"3"))
    ))(i)?;
    let (i, _) = tag(b",")(i)?;
    let (i, payload) = take_until(b"," as &[u8])(i)?;
    let (i, _) = tag(b",")(i)?;
    let (i, fill) = branch::alt((
        value(0u8, tag(b"0")),
        value(1u8, tag(b"1")),
        value(2u8, tag(b"2")),
        value(3u8, tag(b"3")),
        value(4u8, tag(b"4")),
        value(5u8, tag(b"5"))
    ))(i)?;
    let (i, _) = tag(b"*")(i)?;
    let (i, checksum) = take(2u8)(i)?;
    Ok((i, PacketNMEA {
        nmea_type,
        fragment_count,
        fragment_number,
        sequence_id,
        channel,
        payload,
        fill,
        checksum,
    }))
}


#[cfg(test)]
mod tests {
    use super::*;

    fn test_parse_tag_ok(input: &[u8], remaining: &[u8], expected: Option<PacketTag>) {
        let right = Ok((remaining, expected));
        assert_eq!(opt(parse_tag)(input), right);
    }

    #[test]
    fn parse_tag_empty_line() {
        let input = b"";
        let right = None;
        test_parse_tag_ok(input, b"", right);
    }

    #[test]
    fn parse_tag_empty_tag() {
        let input = b"\\*00\\"; // TODO valid input
        let right = Some(PacketTag{ checksum: b"00", fields: vec![] });
        test_parse_tag_ok(input, b"", right);
    }

    #[test]
    fn parse_tag_timestamp() {
        let input = b"\\c:123456*00\\"; // TODO valid input
        let right = Some(PacketTag{ checksum: b"00", fields: vec![PacketTagField{ name: b"c", value: b"123456" }] });
        test_parse_tag_ok(input, b"", right);
    }

    #[test]
    fn parse_tag_source() {
        let input = b"\\s:some_source*00\\"; // TODO valid input
        let right = Some(PacketTag{ checksum: b"00", fields: vec![PacketTagField{ name: b"s", value: b"some_source" }] });
        test_parse_tag_ok(input, b"", right);
    }

    #[test]
    fn parse_tag_timestamp_source() {
        let input = b"\\c:123456,s:some_source*00\\"; // TODO valid input
        let right = Some(PacketTag{ checksum: b"00", fields: vec![PacketTagField{ name: b"c", value: b"123456" }, PacketTagField{ name: b"s", value: b"some_source" }] });
        test_parse_tag_ok(input, b"", right);
    }

    // #[test]
    // fn parse_tag_malformed_not_ended() {
    //     let input = b"\\";
    //     let right: Result<(Option<PacketTag<'_>>, &str), String> = Err(String::from("unexpected end"));
    //     assert_eq!(res, right);
    // }
}
