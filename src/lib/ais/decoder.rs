use super::c::{self, packet_t};
use super::parser::{Packet, PacketTag};
use super::types::*;


pub struct Decoder {
    pub ais: c::ais_t,
    pub contexts: [c::aivdm_context_t; 4],
}


pub fn new() -> Decoder {
    let ais = c::ais_t {
        type_: 0,
        repeat: 0,
        mmsi: 0,
        __bindgen_anon_1: c::ais_t__bindgen_ty_1 {
            type1: c::ais_t__bindgen_ty_1__bindgen_ty_1 {
                status: 0,
                turn: 0,
                speed: 0,
                accuracy: false,
                lon: 0,
                lat: 0,
                course: 0,
                heading: 0,
                second: 0,
                maneuver: 0,
                raim: false,
                radio: 0,
            }
        },
    };
    let contexts = [
        new_aivdm_context_t(),
        new_aivdm_context_t(),
        new_aivdm_context_t(),
        new_aivdm_context_t(),
    ];

    Decoder { ais, contexts }
}


impl Decoder {

    pub fn decode_line(&self, line: &str) -> Result<Option<Message>, DecodeError> {
        let mut packet = c::packet_t::new();
        let mut decoder = new();

        match super::parser::parse(line.as_bytes()) {
            Err(e) => Err(DecodeError::ParseError(e)),
            Ok(p) => {
                set_packet(&p, &mut packet);
                decode(&mut decoder.contexts, &mut decoder.ais, &p.tag, &mut packet)
            },
        }
    }

}


fn new_aivdm_context_t() -> c::aivdm_context_t {
    c::aivdm_context_t {
        decoded_frags: 0,
        bits: [0; 2048],
        bitlen: 0,
        type24_queue: c::ais_type24_queue_t {
            ships: [c::ais_type24a_t{ mmsi: 0, shipname: [0;21] };8],
            index: 0,
        },
    }
}


impl c::packet_t {

    pub fn new() -> c::packet_t {
        c::packet_t {
            timestamp: 0,
            destination: c::string_len_t {
                str_: std::ptr::null(),
                len: 0,
            },
            source: c::string_len_t {
                str_: std::ptr::null(),
                len: 0,
            },
            nmea: c::string_len_t {
                str_: std::ptr::null(),
                len: 0,
            },
            packet_type: c::string_len_t {
                str_: std::ptr::null(),
                len: 0,
            },
            fragment_count: 0,
            fragment_number: 0,
            sequence_id: c::string_len_t {
                str_: std::ptr::null(),
                len: 0,
            },
            channel: 0,
            data: c::string_len_t {
                str_: std::ptr::null(),
                len: 0,
            },
            fill: 0,
            checksum: std::ptr::null(),
        }
    }

}


impl Default for c::packet_t {

    fn default() -> Self {
        Self::new()
    }

}


pub fn set_packet(p: &Packet, packet: &mut packet_t) {
    packet.packet_type = c::string_len_t {
        str_: p.nmea.nmea_type.as_ptr() as *const std::ffi::c_char,
        len: p.nmea.nmea_type.len(),
    };
    packet.fragment_count = std::str::from_utf8(p.nmea.fragment_count).unwrap().parse().unwrap();
    packet.fragment_number = std::str::from_utf8(p.nmea.fragment_number).unwrap().parse().unwrap();
    packet.sequence_id = c::string_len_t {
        str_: p.nmea.sequence_id.as_ptr() as *const std::ffi::c_char,
        len: p.nmea.sequence_id.len(),
    };
    packet.channel = p.nmea.channel as u32;
    packet.data = c::string_len_t {
        str_: p.nmea.payload.as_ptr() as *const std::ffi::c_char,
        len: p.nmea.payload.len(),
    };
    packet.fill = p.nmea.fill;
    packet.checksum = [p.nmea.checksum[0], p.nmea.checksum[1], 0].as_ptr() as *const std::ffi::c_char;
}


#[derive(Debug)]
pub enum DecodeError {
    ParseError(String),
    PacketOutOfOrder,
    PacketTooManyBits,
}


pub fn decode(
    ais_contexts: &mut [c::aivdm_context_t;4],
    ais: &mut c::ais_t,
    packet_tag: &Option<PacketTag>,
    packet: &mut packet_t,
) -> Result<Option<Message>, DecodeError> {
    unsafe {
        match c::decode(ais_contexts.as_mut_ptr(), ais, packet) {
            c if c == c::decoder_result_t_DECODER_OK => Ok(Some(Message::from_c(packet_tag, ais))),
            c if c == c::decoder_result_t_DECODER_PARTIAL => Ok(None),
            c if c == c::decoder_result_t_DECODER_PACKET_OUT_OF_ORDER => Err(DecodeError::PacketOutOfOrder),
            c if c == c::decoder_result_t_DECODER_PACKET_TOO_MANY_BITS => Err(DecodeError::PacketTooManyBits),
            _ => unreachable!(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::lib::ais::types::AISMessage;
    use crate::lib::ais::parser;

    fn bytes_to_str<'a, const N: usize>(data: &[std::os::raw::c_char; N]) -> anyhow::Result<&'a str> {
        Ok(unsafe { std::ffi::CStr::from_ptr(data.as_ptr()) }.to_str()?)
    }

    #[test]
    fn test_packet_1() -> Result<(), DecodeError> {
        let data = r"\s:2573238,c:1737039134*08\!BSVDM,1,1,,B,13mA7?001=PGfT@RS?T28QdJ0D1q,0*58";
        let decoder = new();

        if let Some(msg) = decoder.decode_line(data)? {
            assert!(matches!(msg.message, AISMessage::Type1_2_3 { .. }));
            assert_eq!(msg.mmsi, 257181500);
            if let AISMessage::Type1_2_3 { message_type, status, turn, speed, accuracy, lon, lat, course, heading, second, maneuver, raim, radio, .. } = msg.message {
                assert_eq!(message_type, 1);
                assert_eq!(status, 0);
                assert_eq!(turn, 0);
                assert_eq!(speed, 77);
                assert_eq!(accuracy, true);
                assert_eq!(lon, 3110024);
                assert_eq!(lat, 36229008);
                assert_eq!(course, 546);
                assert_eq!(heading, 54);
                assert_eq!(second, 13);
                assert_eq!(maneuver, 0);
                assert_eq!(raim, false);
                assert_eq!(radio, 82041);
            }
        } else {
            panic!("Expected a message!");
        }

        return Ok(())
    }

    #[test]
    fn test_packet_2() -> Result<(), DecodeError> {
        let data = r"\s:2573205,c:1737039140*05\!BSVDM,1,1,,B,23aO?qi000P1wNvNbTQkJmjP05k@,0*5B";
        let decoder = new();

        if let Some(msg) = decoder.decode_line(data)? {
            assert!(matches!(msg.message, AISMessage::Type1_2_3 { .. }));
            assert_eq!(msg.mmsi, 244830183);
            if let AISMessage::Type1_2_3 { message_type, status, turn, speed, accuracy, lon, lat, course, heading, second, maneuver, raim, radio, .. } = msg.message {
                assert_eq!(message_type, 2);
                assert_eq!(status, 1);
                assert_eq!(turn, 0);
                assert_eq!(speed, 0);
                assert_eq!(accuracy, true);
                assert_eq!(lon, 261087);
                assert_eq!(lat, 32154759);
                assert_eq!(course, 875);
                assert_eq!(heading, 185);
                assert_eq!(second, 16);
                assert_eq!(maneuver, 0);
                assert_eq!(raim, false);
                assert_eq!(radio, 23760);
            }
        } else {
            panic!("Expected a message!");
        }

        return Ok(())
    }

    #[test]
    fn test_packet_3() -> Result<(), DecodeError> {
        let data = r"\s:2573415,c:1737039134*01\!BSVDM,1,1,,B,33n4Vj;Oh2Pr52nV68h=SHLJ015i,0*72";
        let decoder = new();

        if let Some(msg) = decoder.decode_line(data)? {
            assert!(matches!(msg.message, AISMessage::Type1_2_3 { .. }));
            assert_eq!(msg.mmsi, 258025160);
            if let AISMessage::Type1_2_3 { message_type, status, turn, speed, accuracy, lon, lat, course, heading, second, maneuver, raim, radio, .. } = msg.message {
                assert_eq!(message_type, 3);
                assert_eq!(status, 11);
                assert_eq!(turn, 127);
                assert_eq!(speed, 2);
                assert_eq!(accuracy, true);
                assert_eq!(lon, 7612507);
                assert_eq!(lat, 39946432);
                assert_eq!(course, 3469);
                assert_eq!(heading, 270);
                assert_eq!(second, 13);
                assert_eq!(maneuver, 0);
                assert_eq!(raim, false);
                assert_eq!(radio, 4465);
            }
        } else {
            panic!("Expected a message!");
        }

        return Ok(())
    }

    #[test]
    fn test_packet_5() -> anyhow::Result<()> {
        let data = [
            r"\s:2573135,c:1737039134*06\!BSVDM,2,1,7,A,53m@9r400000hKG;WN18E<=DF08E8LE<Dr0BqVpn186314rdR0531`<<d<M`,0*04",
            r"\s:2573135,c:1737039134*06\!BSVDM,2,2,7,A,88888888880,2*3A"
            ];

        let parsed_packet_0 = parser::parse(data[0]
            .as_bytes())
            .map_err(|_| anyhow::anyhow!("Unable to parse first message"))?;
        let parsed_packet_1 = parser::parse(data[1]
            .as_bytes())
            .map_err(|_| anyhow::anyhow!("Unable to parse second message"))?;

        let mut packet = packet_t::new();
        let mut decoder = new();

        set_packet(&parsed_packet_0, &mut packet);
        let decoded_msg_0 = decode(&mut decoder.contexts, &mut decoder.ais, &parsed_packet_0.tag, &mut packet)
            .map_err(|_| anyhow::anyhow!("Unable to decode first message"))?;
        set_packet(&parsed_packet_1, &mut packet);
        let decoded_msg_1 = decode(&mut decoder.contexts, &mut decoder.ais, &parsed_packet_1.tag, &mut packet)
            .map_err(|_| anyhow::anyhow!("Unable to decode second message"))?;

        assert!(decoded_msg_0.is_none());
        if let Some(msg) = decoded_msg_1 {
            assert!(matches!(msg.message, AISMessage::Type5 { .. }));
            assert_eq!(msg.mmsi, 257165800);
            if let AISMessage::Type5 { ais_version, imo, callsign, shipname, shiptype, to_bow, to_stern, to_port, to_starboard, epfd, month, day, hour, minute, draught, destination, dte, .. } = msg.message {
                assert_eq!(ais_version, 1);
                assert_eq!(imo, 0);
                assert_eq!(bytes_to_str(&callsign)?, "LF5297");
                assert_eq!(bytes_to_str(&shipname)?, "RESCUE BERGESEN D.Y.");
                assert_eq!(shiptype, 54);
                assert_eq!(to_bow, 9);
                assert_eq!(to_stern, 6);
                assert_eq!(to_port, 3);
                assert_eq!(to_starboard, 1);
                assert_eq!(epfd, 1);
                assert_eq!(month, 3);
                assert_eq!(day, 21);
                assert_eq!(hour, 12);
                assert_eq!(minute, 34);
                assert_eq!(draught, 0);
                assert_eq!(bytes_to_str(&destination)?, "TLF 02016");
                assert_eq!(dte, false);
            }
        } else {
            panic!("Expected a message!");
        }

        return Ok(())
    }
}
