use std::os::raw::c_char;

use serde::Serialize;

use super::{c, parser::PacketTag};

use crate::utils::serialisation;

pub type MMSI = u32;

#[derive(Clone, Serialize)]
pub struct Message {
    #[serde(flatten, with = "serialisation::tags")]
    pub tag: Vec<(Vec<u8>, Vec<u8>)>,
    #[serde(skip_serializing)]
    pub repeat: u8,
    pub mmsi: u32,
    #[serde(flatten)]
    pub message: AISMessage,
}

impl Message {
    pub unsafe fn from_c(tag: &Option<PacketTag>, ais: &c::ais_t) -> Message {
        let message = match ais.type_ {
            t if (1..=3).contains(&t) => AISMessage::Type1_2_3 {
                message_type: t as u8,
                status: ais.__bindgen_anon_1.type1.status as u8,
                turn: ais.__bindgen_anon_1.type1.turn as u8,
                speed: ais.__bindgen_anon_1.type1.speed as u16,
                accuracy: ais.__bindgen_anon_1.type1.accuracy,
                lon: ais.__bindgen_anon_1.type1.lon,
                lat: ais.__bindgen_anon_1.type1.lat,
                course: ais.__bindgen_anon_1.type1.course as u16,
                heading: ais.__bindgen_anon_1.type1.heading as u16,
                second: ais.__bindgen_anon_1.type1.second as u8,
                maneuver: ais.__bindgen_anon_1.type1.maneuver as u8,
                raim: ais.__bindgen_anon_1.type1.raim,
                radio: ais.__bindgen_anon_1.type1.radio,
            },
            4 => AISMessage::Type4 {},
            5 => AISMessage::Type5 {
                ais_version: ais.__bindgen_anon_1.type5.ais_version as u8,
                imo: ais.__bindgen_anon_1.type5.imo,
                callsign: ais.__bindgen_anon_1.type5.callsign,
                shipname: ais.__bindgen_anon_1.type5.shipname,
                shiptype: ais.__bindgen_anon_1.type5.shiptype as u8,
                to_bow: ais.__bindgen_anon_1.type5.to_bow as u16,
                to_stern: ais.__bindgen_anon_1.type5.to_stern as u16,
                to_port: ais.__bindgen_anon_1.type5.to_port as u8,
                to_starboard: ais.__bindgen_anon_1.type5.to_starboard as u8,
                epfd: ais.__bindgen_anon_1.type5.epfd as u8,
                month: ais.__bindgen_anon_1.type5.month as u8,
                day: ais.__bindgen_anon_1.type5.day as u8,
                hour: ais.__bindgen_anon_1.type5.hour as u8,
                minute: ais.__bindgen_anon_1.type5.minute as u8,
                draught: ais.__bindgen_anon_1.type5.draught as u8,
                destination: ais.__bindgen_anon_1.type5.destination,
                dte: ais.__bindgen_anon_1.type5.dte != 0, // bool
            },
            6 => AISMessage::Type6 {},
            7 => AISMessage::Type7 {},
            8 => AISMessage::Type8 {},
            9 => AISMessage::Type9 {},
            10 => AISMessage::Type10 {},
            11 => AISMessage::Type11 {},
            12 => AISMessage::Type12 {},
            13 => AISMessage::Type13 {},
            14 => AISMessage::Type14 {},
            15 => AISMessage::Type15 {},
            16 => AISMessage::Type16 {},
            17 => AISMessage::Type17 {},
            18 => AISMessage::Type18 {
                reserved: ais.__bindgen_anon_1.type18.reserved as u8,
                speed: ais.__bindgen_anon_1.type18.speed as u16,
                accuracy: ais.__bindgen_anon_1.type18.accuracy,
                lon: ais.__bindgen_anon_1.type18.lon,
                lat: ais.__bindgen_anon_1.type18.lat,
                course: ais.__bindgen_anon_1.type18.course as u16,
                heading: ais.__bindgen_anon_1.type18.heading as u16,
                second: ais.__bindgen_anon_1.type18.second as u8,
                regional: ais.__bindgen_anon_1.type18.regional as u8,
                cs: ais.__bindgen_anon_1.type18.cs,
                display: ais.__bindgen_anon_1.type18.display,
                dsc: ais.__bindgen_anon_1.type18.dsc,
                band: ais.__bindgen_anon_1.type18.band,
                msg22: ais.__bindgen_anon_1.type18.msg22,
                assigned: ais.__bindgen_anon_1.type18.assigned,
                raim: ais.__bindgen_anon_1.type18.raim,
                radio: ais.__bindgen_anon_1.type18.radio,
            },
            19 => AISMessage::Type19 {
                reserved: ais.__bindgen_anon_1.type19.reserved as u8,
                speed: ais.__bindgen_anon_1.type19.speed as u16,
                accuracy: ais.__bindgen_anon_1.type19.accuracy,
                lon: ais.__bindgen_anon_1.type19.lon,
                lat: ais.__bindgen_anon_1.type19.lat,
                course: ais.__bindgen_anon_1.type19.course as u16,
                heading: ais.__bindgen_anon_1.type19.heading as u16,
                second: ais.__bindgen_anon_1.type19.second as u8,
                regional: ais.__bindgen_anon_1.type19.regional as u8,
                shipname: ais.__bindgen_anon_1.type19.shipname,
                shiptype: ais.__bindgen_anon_1.type19.shiptype as u8,
                to_bow: ais.__bindgen_anon_1.type19.to_bow as u16,
                to_stern: ais.__bindgen_anon_1.type19.to_stern as u16,
                to_port: ais.__bindgen_anon_1.type19.to_port as u8,
                to_starboard: ais.__bindgen_anon_1.type19.to_starboard as u8,
                epfd: ais.__bindgen_anon_1.type19.epfd as u8,
                raim: ais.__bindgen_anon_1.type19.raim,
                dte: ais.__bindgen_anon_1.type19.dte == 1,
                assigned: ais.__bindgen_anon_1.type19.assigned,
            },
            20 => AISMessage::Type20 {},
            21 => AISMessage::Type21 {
                aid_type: ais.__bindgen_anon_1.type21.aid_type as u8,
                name: ais.__bindgen_anon_1.type21.name,
                accuracy: ais.__bindgen_anon_1.type21.accuracy,
                lon: ais.__bindgen_anon_1.type21.lon,
                lat: ais.__bindgen_anon_1.type21.lat,
                to_bow: ais.__bindgen_anon_1.type21.to_bow as u16,
                to_stern: ais.__bindgen_anon_1.type21.to_stern as u16,
                to_port: ais.__bindgen_anon_1.type21.to_port as u8,
                to_starboard: ais.__bindgen_anon_1.type21.to_starboard as u8,
                epfd: ais.__bindgen_anon_1.type21.epfd as u8,
                second: ais.__bindgen_anon_1.type21.second as u8,
                off_position: ais.__bindgen_anon_1.type21.off_position,
                regional: ais.__bindgen_anon_1.type21.regional as u8,
                raim: ais.__bindgen_anon_1.type21.raim,
                virtual_aid: ais.__bindgen_anon_1.type21.virtual_aid,
                assigned: ais.__bindgen_anon_1.type21.assigned,
            },
            22 => AISMessage::Type22 {},
            23 => AISMessage::Type23 {},
            24 => AISMessage::Type24 {
                part: match ais.__bindgen_anon_1.type24.part {
                    c::part_t_both => Type24Part::Both,
                    c::part_t_part_a => Type24Part::PartA,
                    c::part_t_part_b => Type24Part::PartB,
                    n => {
                        crate::log::log_warning(format!(
                            "lib::ais::from_c: unknwown type 24 part '{}'",
                            n
                        ));
                        Type24Part::Unknown(n)
                    }
                },
                // part A
                shipname: ais.__bindgen_anon_1.type24.shipname,
                // part B
                shiptype: ais.__bindgen_anon_1.type24.shiptype as u8,
                vendorid: ais.__bindgen_anon_1.type24.vendorid,
                model: ais.__bindgen_anon_1.type24.model as u8,
                serial: ais.__bindgen_anon_1.type24.serial,
                callsign: ais.__bindgen_anon_1.type24.callsign,
                extra: if 980000000 <= ais.mmsi && ais.mmsi < 990000000 {
                    Type24Extra::Auxiliary {
                        mothership_mmsi: ais
                            .__bindgen_anon_1
                            .type24
                            .__bindgen_anon_1
                            .mothership_mmsi,
                    }
                } else {
                    Type24Extra::Main {
                        to_bow: ais.__bindgen_anon_1.type24.__bindgen_anon_1.dim.to_bow as u16,
                        to_stern: ais.__bindgen_anon_1.type24.__bindgen_anon_1.dim.to_stern as u16,
                        to_port: ais.__bindgen_anon_1.type24.__bindgen_anon_1.dim.to_port as u8,
                        to_starboard: ais
                            .__bindgen_anon_1
                            .type24
                            .__bindgen_anon_1
                            .dim
                            .to_starboard as u8,
                    }
                },
            },
            25 => AISMessage::Type25 {},
            26 => AISMessage::Type26 {},
            27 => AISMessage::Type27 {},
            t => {
                eprintln!("UNREACHABLE CODE: type {}", t);
                unreachable!()
            }
        };
        let message_tag = match tag {
            Some(tag) => tag.fields.iter().map(|f| f.as_tuple()).collect(),
            None => Vec::new(),
        };
        Message {
            tag: message_tag,
            repeat: ais.repeat as u8,
            mmsi: ais.mmsi,
            message, // TODO
        }
    }

    pub fn message_type(&self) -> u8 {
        match self.message {
            AISMessage::Type1_2_3 { message_type, .. } => message_type,
            AISMessage::Type4 { .. } => 4,
            AISMessage::Type5 { .. } => 5,
            AISMessage::Type6 { .. } => 6,
            AISMessage::Type7 { .. } => 7,
            AISMessage::Type8 { .. } => 8,
            AISMessage::Type9 { .. } => 9,
            AISMessage::Type10 { .. } => 10,
            AISMessage::Type11 { .. } => 11,
            AISMessage::Type12 { .. } => 12,
            AISMessage::Type13 { .. } => 13,
            AISMessage::Type14 { .. } => 14,
            AISMessage::Type15 { .. } => 15,
            AISMessage::Type16 { .. } => 16,
            AISMessage::Type17 { .. } => 17,
            AISMessage::Type18 { .. } => 18,
            AISMessage::Type19 { .. } => 19,
            AISMessage::Type20 { .. } => 20,
            AISMessage::Type21 { .. } => 21,
            AISMessage::Type22 { .. } => 22,
            AISMessage::Type23 { .. } => 23,
            AISMessage::Type24 { .. } => 24,
            AISMessage::Type25 { .. } => 25,
            AISMessage::Type26 { .. } => 26,
            AISMessage::Type27 { .. } => 27,
        }
    }
}

#[derive(Clone, PartialEq, Eq, Serialize)]
#[serde(tag = "type")]
pub enum AISMessage {
    /* Type1_2_3 needs to be at the end as it is a serde untagged variant */
    Type4 {/* TODO */},
    #[serde(rename = "5")]
    Type5 {
        ais_version: u8, // AIS version level // TODO enum
        imo: u32,        // IMO identification
        #[serde(with = "serialisation::char_array")]
        callsign: [c_char; 7 + 1], // callsign
        #[serde(with = "serialisation::char_array")]
        shipname: [c_char; 20 + 1], // vessel name
        shiptype: u8,    // ship type code
        to_bow: u16,     // dimension to bow
        to_stern: u16,   // dimension to stern
        to_port: u8,     // dimension to port
        to_starboard: u8, // dimension to starboard
        epfd: u8,        // type of position fix device // TODO enum
        month: u8,       // UTC month
        day: u8,         // UTC day
        hour: u8,        // UTC hour
        minute: u8,      // UTC minute
        draught: u8,     // draft in meters
        #[serde(with = "serialisation::char_array")]
        destination: [c_char; 20 + 1], // ship destination
        dte: bool,       // data terminal enable
    },
    Type6 {/* TODO */},
    Type7 {/* TODO */},
    Type8 {/* TODO */},
    Type9 {/* TODO */},
    Type10 {/* TODO */},
    Type11 {/* TODO */},
    Type12 {/* TODO */},
    Type13 {/* TODO */},
    Type14 {/* TODO */},
    Type15 {/* TODO */},
    Type16 {/* TODO */},
    Type17 {/* TODO */},
    #[serde(rename = "18")]
    Type18 {
        reserved: u8, // (?) altitude in meters
        #[serde(with = "serialisation::speed::deciknots")]
        speed: u16, // speed over ground in deciknots
        accuracy: bool, // position accuracy
        #[serde(with = "serialisation::position::lon_deg")]
        lon: i32, // longitude
        #[serde(with = "serialisation::position::lat_deg")]
        lat: i32, // latitude
        #[serde(with = "serialisation::course::tenth_of_deg")]
        course: u16, // course over ground
        #[serde(with = "serialisation::heading")]
        heading: u16, // true heading
        second: u8,
        regional: u8,   // regional reserved
        cs: bool,       // carrier sense unit flag // TODO enum
        display: bool,  // unit has attached display? // TODO enum
        dsc: bool,      // unit attached to radio with DSC?
        band: bool,     // unit can switch frequency bands?
        msg22: bool,    // can accept Message 22 management?
        assigned: bool, // assigned-mode flag
        raim: bool,     // RAIM flag
        radio: u32,     // radio status bits
    },
    #[serde(rename = "19")]
    Type19 {
        reserved: u8, // altitude in meters
        #[serde(with = "serialisation::speed::deciknots")]
        speed: u16, // speed over ground in deciknots
        accuracy: bool, // position accuracy
        #[serde(with = "serialisation::position::lon_deg")]
        lon: i32, // longitude
        #[serde(with = "serialisation::position::lat_deg")]
        lat: i32, // latitude
        #[serde(with = "serialisation::course::tenth_of_deg")]
        course: u16, // course over ground
        #[serde(with = "serialisation::heading")]
        heading: u16, // true heading
        second: u8,
        regional: u8, // regional reserved
        #[serde(with = "serialisation::char_array")]
        shipname: [c_char; 20 + 1], // ship name
        shiptype: u8, // ship type code
        to_bow: u16,  // dimension to bow
        to_stern: u16, // dimension to stern
        to_port: u8,  // dimension to port
        to_starboard: u8, // dimension to starboard
        epfd: u8,     // type of position fix device
        raim: bool,   // RAIM flag
        dte: bool,    // date terminal enable
        assigned: bool, // assigned-mode flag
    },
    Type20 {/* TODO */},
    #[serde(rename = "21")]
    Type21 {
        aid_type: u8, // aid type // TODO enum
        #[serde(with = "serialisation::char_array")]
        name: [c_char; 35], // name of aid to navigation
        accuracy: bool, // position accuracy
        #[serde(with = "serialisation::position::lon_deg")]
        lon: i32, // longitude
        #[serde(with = "serialisation::position::lat_deg")]
        lat: i32, // latitude
        to_bow: u16,  // dimension to bow
        to_stern: u16, // dimension to stern
        to_port: u8,  // dimension to port
        to_starboard: u8, // dimension to starboard
        epfd: u8,     // type of EPFD
        second: u8,
        off_position: bool, // off-position indicator
        regional: u8,       // regional reserved field
        raim: bool,         // RAIM flag
        virtual_aid: bool,  // is virtual station?
        assigned: bool,     // assigned-mode flag
    },
    Type22 {/* TODO */},
    Type23 {/* TODO */},
    #[serde(rename = "24")]
    Type24 {
        part: Type24Part,
        // part A
        #[serde(with = "serialisation::char_array")]
        shipname: [c_char; 20 + 1], // vessel name
        // part B
        shiptype: u8, // ship type code
        #[serde(with = "serialisation::char_array")]
        vendorid: [c_char; 7 + 1], // vendor ID // TODO it should only be a 3 six-bit char array
        model: u8,    // unit model code
        serial: u32,  // serial number
        #[serde(with = "serialisation::char_array")]
        callsign: [c_char; 7 + 1], // callsign
        extra: Type24Extra,
    },
    Type25 {/* TODO */},
    Type26 {/* TODO */},
    Type27 {/* TODO */},
    /* all variants with the #[serde(untagged)] attribute must be placed at the end of the enum */
    #[serde(untagged)]
    Type1_2_3 {
        #[serde(rename = "type", with = "serialisation::transform::number_to_str")]
        message_type: u8,
        status: u8, // navigation status // TODO enum
        turn: u8,   // rate of turn
        #[serde(with = "serialisation::speed::deciknots")]
        speed: u16, // speed over ground in deciknots
        accuracy: bool, // position accuracy // TODO enum?
        #[serde(with = "serialisation::position::lon_deg")]
        lon: i32, // longitude
        #[serde(with = "serialisation::position::lat_deg")]
        lat: i32, // latitude
        #[serde(with = "serialisation::course::tenth_of_deg")]
        course: u16, // course over ground
        #[serde(with = "serialisation::heading")]
        heading: u16, // true heading
        second: u8, // UTC second when the report was generated by the electronic position system (EPFS)
        maneuver: u8, // maneuver indicator // TODO enum
        raim: bool, // Receiver Autonomous Integrity Monitoring (RAIM) flag of electronic position fixing device // TODO enum
        radio: u32, // radio status bits
    },
}

#[derive(Clone, PartialEq, Eq, Serialize)]
pub enum Type24Part {
    Both,
    PartA,
    PartB,
    Unknown(::std::os::raw::c_uint),
}

#[derive(Clone, PartialEq, Eq, Serialize)]
pub enum Type24Extra {
    Auxiliary {
        mothership_mmsi: u32, // MMSI of main vessel
    },
    Main {
        to_bow: u16,      // dimension to bow
        to_stern: u16,    // dimension to stern
        to_port: u8,      // dimension to port
        to_starboard: u8, // dimension to starboard
    },
}
