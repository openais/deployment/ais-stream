#include <string.h>

#include "decoder.h"


static inline bool decode_bits (struct aivdm_context_t * ais_context, Packet * packet) {
    // From https://gitlab.com/gpsd/gpsd/-/blob/3a243b1a5c7a5dae633f45753d07c3220ab90510/drivers/drivers.c
    /*
     * This function is Copyright 2010 by the GPSD project
     * SPDX-License-Identifier: BSD-2-clause
     */
    // wacky 6-bit encoding, shades of FIELDATA
    // Max 256 is a guess, to pacify Codacy
    unsigned char * cp;
    unsigned char * end = ((unsigned char *)packet->data.str) + (packet->data.len > 256 ? 256 : packet->data.len);
    for (cp = (unsigned char *)packet->data.str ; cp < end ; cp++) {
        unsigned char ch;
        ch = *cp;
        ch -= 48;
        if (ch >= 40) {
            ch -= 8;
        }
        int i;
        for (i = 5; i >= 0; i--) {
            if ((ch >> i) & 0x01) {
                ais_context->bits[ais_context->bitlen / 8] |=
                    (1 << (7 - ais_context->bitlen % 8));
            }
            ais_context->bitlen++;
            if (ais_context->bitlen > sizeof(ais_context->bits)) {
                // std::cerr << "WARNING Line " << line_number << " too many bits" << std::endl;
                // fprintf(stderr, "WARNING Line %zu too many bits\n", line_number);
                return false; // TODO
            }
        }
    }
    return true;
}


enum decoder_result_t decode (struct aivdm_context_t * ais_contexts, struct ais_t * ais, Packet * packet) {
    struct aivdm_context_t * ais_context;
    ais_context = &ais_contexts[packet->channel];
    if (packet->fragment_number != ais_context->decoded_frags + 1) {
        if (packet->fragment_number != 1) {
            // std::cerr << "WARNING Line " << line_number << " packet out of order" << std::endl;
            // fprintf(stderr, "WARNING Line %zu packet out of order\n", line_number);
            return DECODER_PACKET_OUT_OF_ORDER;
        }
        ais_context->decoded_frags = 0;
    }
    if (1 == packet->fragment_number) {
        memset(ais_context->bits, '\0', sizeof(ais_context->bits));
        ais_context->bitlen = 0;
    }
    if (!decode_bits(ais_context, packet)) {
        return DECODER_PACKET_TOO_MANY_BITS;
    }
    ais_context->bitlen -= packet->fill;
    if (packet->fragment_number == packet->fragment_count) {
        // clear waiting fragments count
        ais_context->decoded_frags = 0;

        // decode the assembled binary packet
        ais_binary_decode(ais, ais_context->bits, ais_context->bitlen, &ais_context->type24_queue);

        // std::cout
        //     << "Line " << line_number << ": "
        //     << "AIS type " << ais->type
        //     << ": MMSI=" << ais->mmsi
        //     << std::endl;

        //fprintf(stdout, "Line %zu: AIS type %u: MMSI=%u\n", line_number, ais->type, ais->mmsi);

        return DECODER_OK;
    }

    ais_context->decoded_frags++;

    return DECODER_PARTIAL;
}
