#ifndef AIS_DECODER_H
#define AIS_DECODER_H

#include "gpsd/driver_ais.h"

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

typedef struct string_len_t {
    const char * str;
    size_t len;
} string_len_t;

enum radio_channel_t {
    RADIO_NONE = 0,
    RADIO_A = 1,
    RADIO_B = 2,
    RADIO_C = 3,
};

typedef struct packet_t {
    // NMEA Tag Fields
    uint64_t timestamp;
    string_len_t destination;
    string_len_t source;
    // NMEA Message
    string_len_t nmea;
    string_len_t packet_type;
    size_t fragment_count;
    size_t fragment_number;
    string_len_t sequence_id;
    enum radio_channel_t channel;
    string_len_t data;
    uint8_t fill;
    const char * checksum;
} Packet;

enum decoder_result_t {
    DECODER_OK,
    DECODER_PARTIAL,
    DECODER_PACKET_OUT_OF_ORDER,
    DECODER_PACKET_TOO_MANY_BITS,
};

enum decoder_result_t decode (struct aivdm_context_t * ais_contexts, struct ais_t * ais, Packet * packet);

#endif // AIS_DECODER_H
