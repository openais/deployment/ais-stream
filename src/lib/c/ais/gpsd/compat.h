#ifndef COMPAT_H
#define COMPAT_H

/*
 * This file is Copyright (c)2017-2019 by the GPSD project
 * SPDX-License-Identifier: BSD-2-clause
 */
// https://github.com/ukyg9e5r6k7gubiekd6/gpsd/blob/master/os_compat.c

#if __APPLE__
#define HAVE_STRLCPY
#endif

#ifndef HAVE_STRLCPY

#include <string.h>

/*
 * Copy src to string dst of size siz.  At most siz-1 characters
 * will be copied.  Always NUL terminates (unless siz == 0).
 * Returns strlen(src); if retval >= siz, truncation occurred.
 */
size_t strlcpy(char *dst, const char *src, size_t siz)
{
    size_t len = strlen(src);
    if (siz != 0) {
	if (len >= siz) {
	    memcpy(dst, src, siz - 1);
	    dst[siz - 1] = '\0';
	} else
	    memcpy(dst, src, len + 1);
    }
    return len;
}

#endif /* !HAVE_STRLCPY */

#endif // COMPAT_H
