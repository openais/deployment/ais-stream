// From https://gitlab.com/gpsd/gpsd/-/blob/3a243b1a5c7a5dae633f45753d07c3220ab90510/include/compiler.h
/*
 * compiler.h - compiler specific macros
 *
 * This file is Copyright 2010 by the GPSD project
 * SPDX-License-Identifier: BSD-2-clause
 *
 * Calling file needs to have previously included "gpsd_config.h"
 */
#ifndef _GPSD_COMPILER_H_
#define _GPSD_COMPILER_H_


// Macro for declaring function arguments unused.
#if defined(__GNUC__) || defined(__clang__)
    #define UNUSED __attribute__((unused))
#else
    #define UNUSED
#endif

// Macro to suppress FALLTHROUGH warnings
#if defined(__GNUC__) && __GNUC__ >= 7
    #define FALLTHROUGH __attribute__((fallthrough));
// At least one Clang 6 version advertises fallthrough and then chokes on it
#elif defined(__clang__) && __clang_major__ > 6
    #ifndef __has_attribute         // For backwards compatibility
        #define __has_attribute(x) 0
    #endif

    #if __has_attribute(fallthrough)
        #define FALLTHROUGH __attribute__((fallthrough));
    #endif
#endif

#ifndef FALLTHROUGH
    #define FALLTHROUGH
#endif


#ifdef __COVERITY__
    // do nothing
#elif defined(__cplusplus)
    // we are C++
    #if __cplusplus >= 201103L
        /* C++ before C++11 can not handle stdatomic.h or atomic
         * atomic is just C++ for stdatomic.h */
        #include <atomic>
    #endif
#elif defined(HAVE_STDATOMIC_H)
    // we are C and atomics are in C98 and newer
    #include <stdatomic.h>
#elif defined(HAVE_OSATOMIC_H)
    // do it the OS X way
    #include <libkern/OSAtomic.h>
#endif  // HAVE_OSATOMIC_H

// prevent instruction reordering across any call to this function
static inline void memory_barrier(void)
{
#ifdef __COVERITY__
    // do nothing
#elif defined(__cplusplus)
    // we are C++
    #if __cplusplus >= 201103L
        // C++11 and later has atomics, earlier do not
        std::atomic_thread_fence(std::memory_order_seq_cst);
    #endif
#elif defined HAVE_STDATOMIC_H
    // we are C and atomics are in C98 and newer
    atomic_thread_fence(memory_order_seq_cst);
#elif defined(HAVE_OSATOMIC_H)
    // do it the OS X way
    OSMemoryBarrier();
#elif defined(__GNUC__)
    __asm__ __volatile__ ("" : : : "memory");
#endif
}

/* likely(), unlikely(), branch optimization
 * https://kernelnewbies.org/FAQ/LikelyUnlikely
 */
#if defined(__GNUC__)
    #define likely(x)       __builtin_expect(!!(x), 1)
    #define unlikely(x)     __builtin_expect(!!(x), 0)
#else
    #define likely(x)       (x)
    #define unlikely(x)     (x)
#endif

#endif  // _GPSD_COMPILER_H_
// vim: set expandtab shiftwidth=4
