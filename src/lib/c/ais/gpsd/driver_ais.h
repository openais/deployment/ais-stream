// From https://gitlab.com/gpsd/gpsd/-/blob/3a243b1a5c7a5dae633f45753d07c3220ab90510/include/gpsd.h
/* gpsd.h -- fundamental types and structures for the gpsd library
 *
 * Nothing in this file should be used by any client.  So safe to change
 * anything here without (directly) affecting the API or ABI.
 *
 * This file is Copyright 2017 by the GPSD project
 * SPDX-License-Identifier: BSD-2-clause
 */

#ifndef _GPSD_H_
#define _GPSD_H_

# ifdef __cplusplus
extern "C" {
# endif


#include "gps.h"


/*
 * RTCM3 is more variable length than RTCM 2.
 *
 * In theory, the limit is
 *   1 octet preamble
 *   2 octets payload length (first 6 bits reserved) -->
 *       max payload length 1023 octets
 *   0-1023 octects payload
 *   3 octets CRC
 *   1029 octets maximum
 *
 * Use 1040 out of paranoia
 */
#define RTCM3_MAX       1040

#define AIVDM_CHANNELS  2               // A, B

// state for resolving interleaved Type 24 packets
struct ais_type24a_t {
    unsigned int mmsi;
    char shipname[AIS_SHIPNAME_MAXLEN+1];
};
#define MAX_TYPE24_INTERLEAVE   8       // max number of queued type 24s
struct ais_type24_queue_t {
    struct ais_type24a_t ships[MAX_TYPE24_INTERLEAVE];
    uint8_t index;
};

// state for resolving AIVDM decodes
struct aivdm_context_t {
    // hold context for decoding AIDVM packet sequences
    size_t decoded_frags;     // for tracking AIDVM parts in a multipart sequence
    unsigned char bits[2048];
    size_t bitlen;         // how many valid bits
    struct ais_type24_queue_t type24_queue;
};


struct aivdm_t {
    struct aivdm_context_t context[AIVDM_CHANNELS];
    char ais_channel;
};


extern bool ais_binary_decode (
    struct ais_t *ais,
    const unsigned char *, size_t,
    struct ais_type24_queue_t *);

# ifdef __cplusplus
}
# endif

#endif  // _GPSD_H_
// Local variables:
// mode: c
// end:
// vim: set expandtab shiftwidth=4
