pub mod c {
    #![allow(non_upper_case_globals)]
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]
    #![allow(clippy::approx_constant)]
    #![allow(clippy::useless_transmute)]
    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}
pub mod decoder;
pub mod parser;
pub mod state;
pub mod types;
