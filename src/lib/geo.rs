use std::path::Path;

use geos::{CoordSeq, Geom, Geometry};
use rstar::{primitives::GeomWithData, RTree};
use geo_types::*;
use wkt::TryFromWkt;


pub fn in_region(region: &Geometry, lat: i32, lon: i32) -> bool {
    let flon: f64 = lon as f64 / 600000.0;
    let flat: f64 = lat as f64 / 600000.0;
    let coord_seq = CoordSeq::new_from_vec(&[&[flat, flon]]).expect("CoordSeq");
    let point = Geometry::create_point(coord_seq).expect("geometry");
    region.intersects(&point).expect("in_region intersects")
}


pub type GridCell = GeomWithData<geo_types::Polygon, i32>;


fn make_grid_cell(id: i32, geom: &str) -> GeomWithData<geo_types::Polygon, i32> {
    GeomWithData::new(Polygon::try_from_wkt_str(geom).unwrap(), id)
}


pub fn load_grid_from_csv<P>(path: P) -> Result<RTree<GridCell>, Box<dyn std::error::Error>>
where
    P: AsRef<Path>
{
    let mut elements: Vec<GridCell> = Vec::new();
    let mut reader = csv::Reader::from_path(path)?;
    for record in reader.records() {
        let record = record?;
        let element = make_grid_cell(record[0].parse()?, &record[1]);
        elements.push(element);
    }
    Ok(RTree::bulk_load(elements))
}


pub type Line = (i32, geo_types::LineString);


pub fn load_lines_from_csv<P>(path: P) -> Result<Vec<Line>, Box<dyn std::error::Error>>
where
    P: AsRef<Path>
{
    let mut elements: Vec<Line> = Vec::new();
    let mut reader = csv::Reader::from_path(path)?;
    for record in reader.records() {
        let record = record?;
        elements.push((record[0].parse()?, geo_types::LineString::try_from_wkt_str(&record[1]).unwrap()));
    }
    Ok(elements)
}
