use std::{
    process::exit,
    sync::{atomic::AtomicBool, Arc, Condvar, Mutex},
};

use clap::Parser;
use prometheus_client::registry::Registry;
use serde_json;
use signal_hook::{consts::TERM_SIGNALS, flag, iterator::Signals};

use ais_stream::{
    db::{
        postgresql::{self, PostgreSQLDBRunner},
        sqlite::{self, SQLiteDB, SQLiteDBRunner},
        types::DBItem,
    },
    decoder::Decoder,
    file,
    http::metrics::MetricsServer,
    http::server::SSEServer,
    lib::{
        ais::types::Message,
        geo::{load_grid_from_csv, load_lines_from_csv},
    },
    log::*,
    state::State,
    tcp::{client::TCPClient, server::TCPServer},
    utils::messages,
    Stop,
};

const MAX_DECODED_MESSAGES: usize = 1000;
const MAX_RAW_MESSAGES: usize = 1000;
const MAX_FILTERED_MESSAGES: usize = 1000;


fn main() {
    let cli = Cli::parse();

    let config = match Config::new(cli) {
        Ok(config) => config,
        Err(e) => {
            match e {
                ConfigErr::Source(SourceErr::InvalidURL(e)) => {
                    log_error(format!("main: invalid source URL {e}"));
                }
                ConfigErr::Source(SourceErr::InvalidTCPAddress) => {
                    log_error("main: invalid or unsupported source TCP address");
                }
                ConfigErr::Source(SourceErr::UnsupportedURL) => {
                    log_error("main: unsupported source URL");
                }
                ConfigErr::Sink(SinkErr::InvalidURL(e)) => {
                    log_error(format!("main: invalid sink URL {e}"));
                }
                ConfigErr::Sink(SinkErr::InvalidTCPAddress) => {
                    log_error("main: invalid or unsupported sink TCP address");
                }
                ConfigErr::Sink(SinkErr::InvalidHTTPAddress) => {
                    log_error("main: invalid or unsupported sink HTTP address");
                }
                ConfigErr::Sink(SinkErr::UnsupportedURL) => {
                    log_error("main: unsupported sink URL");
                }
                ConfigErr::SinkFormat(e) => {
                    log_error(format!("main: sink format error: {e}"));
                }
            }
            std::process::exit(1);
        }
    };

    let mut registry = Registry::default();
    let mut app = App::new(config.sink_format);

    let decoder = match config.sink_format {
        SinkFormat::Raw => None,
        _ => {
            let decoder = Decoder::spawn(
                app.raw_messages.subscribe_sync(MAX_RAW_MESSAGES),
                app.decoded_messages.as_ref().map(|d| d.sender()).unwrap(),
            );
            decoder.register_metrics(&mut registry);

            Some(decoder)
        }
    };

    let state = match config.sink_format {
        SinkFormat::Filtered => {
            let timestamp = chrono::DateTime::from_timestamp_millis(1000).unwrap();
            let grid = config.grid.map(|grid| load_grid_from_csv(grid).unwrap());
            let geo_lines = config
                .lines
                .map(|lines| load_lines_from_csv(lines).unwrap());
            let state = State::spawn(
                grid,
                geo_lines,
                config.analysis_only,
                app.decoded_messages
                    .as_mut()
                    .map(|d| d.subscribe_sync(MAX_DECODED_MESSAGES))
                    .unwrap(),
                app.filtered_messages.as_ref().map(|f| f.sender()).unwrap(),
                timestamp,
            );
            state.register_metrics(&mut registry);

            Some(state)
        }
        _ => None,
    };

    let mut source: Box<dyn Stop<(), Box<dyn std::any::Any + Send + 'static>>> = match config.source {
        Source::File { ref path } => {
            log_info(format!("main: using file as source: {}", path));
            match file::input::spawn(path, app.raw_messages.sender()) {
                Err(e) => {
                    log_error(format!("main: could not open file '{path}': {e}"));
                    exit(1);
                }
                Ok(thread) => Box::new(thread),
            }
        }
        Source::Tcp { ref address } => {
            log_info(format!("main: using tcp as source: {address}"));
            let c = TCPClient::spawn(address, app.raw_messages.sender());
            c.register_metrics(&mut registry);
            // tcp_client = Some(c);
            Box::new(c)
        }
    };

    let sink: Option<Box<dyn Stop<(), Box<dyn std::any::Any + Send + 'static>>>> = match config.sink {
        None => None,
        Some(Sink::File { path }) => {
            let file: Box<dyn std::io::Write + Send> = match path {
                None => Box::new(std::io::stdout()),
                Some(path) => Box::new(std::fs::File::create(path).unwrap()), // TODO Error handling.
            };
            Some(Box::new(match config.sink_format {
                SinkFormat::Raw => file::output::spawn(
                    file,
                    app.raw_messages.subscribe_sync(MAX_RAW_MESSAGES),
                    |line| {
                        Ok(if line.ends_with("\n") {
                            format!("OUTPUT: {}", line)
                        } else {
                            format!("OUTPUT: {}\n", line)
                        })
                    },
                ),
                SinkFormat::Decoded => file::output::spawn(
                    file,
                    app.decoded_messages
                        .as_mut()
                        .map(|d| d.subscribe_sync(MAX_DECODED_MESSAGES))
                        .unwrap(),
                    |msg| Ok(serde_json::to_string(&msg).map(|json| format!("{json}\n"))?),
                ),
                SinkFormat::Filtered => file::output::spawn(
                    file,
                    app.filtered_messages
                        .as_mut()
                        .map(|f| f.subscribe_sync(MAX_FILTERED_MESSAGES))
                        .unwrap(),
                    |msg| Ok(serde_json::to_string(&msg).map(|json| format!("{json}\n"))?),
                ),
            }))
        }
        Some(Sink::Tcp { address }) => {
            let s = match config.sink_format {
                SinkFormat::Raw => TCPServer::spawn(&address, app.raw_messages.clone()),
                SinkFormat::Decoded => TCPServer::spawn(
                    &address,
                    app.decoded_messages.as_ref().map(|d| d.clone()).unwrap(),
                ),
                SinkFormat::Filtered => TCPServer::spawn(
                    &address,
                    app.filtered_messages.as_ref().map(|f| f.clone()).unwrap(),
                ),
            };
            s.register_metrics(&mut registry);
            Some(Box::new(s))
        }
        Some(Sink::Http { address }) => {
            let s = match config.sink_format {
                SinkFormat::Raw => SSEServer::spawn(&address, app.raw_messages.clone()),
                SinkFormat::Decoded => SSEServer::spawn(
                    &address,
                    app.decoded_messages.as_ref().map(|d| d.clone()).unwrap(),
                ),
                SinkFormat::Filtered => SSEServer::spawn(
                    &address,
                    app.filtered_messages.as_ref().map(|f| f.clone()).unwrap(),
                ),
            };
            s.register_metrics(&mut registry);
            Some(Box::new(s))
        }
        Some(Sink::Sqlite { path }) => {
            let sqlitedb = SQLiteDB::new(path).expect("create SQLite database");
            let sqlitedb_thread = SQLiteDBRunner::spawn(
                sqlitedb,
                app.filtered_messages
                    .as_mut()
                    .map(|f| f.subscribe_sync(sqlite::DB_UPSTREAM_LIMIT * 2))
                    .unwrap(),
            );
            // TODO sqlitedb_thread metrics
            Some(Box::new(sqlitedb_thread))
        }
        Some(Sink::Postgresql { uri }) => {
            let postgresdb_thread = PostgreSQLDBRunner::spawn(
                &uri,
                app.filtered_messages
                    .as_mut()
                    .map(|f| f.subscribe_sync(postgresql::DB_UPSTREAM_LIMIT * 2))
                    .unwrap(),
            );
            Some(Box::new(postgresdb_thread))
        }
    };

    let registry = Arc::new(registry);
    // TODO Do not start metrics server by default.
    // TODO CLI option to start the metrics server with the option to set the binding address and port.
    let metrics_server: Option<MetricsServer> = Some(MetricsServer::spawn(&String::from("0.0.0.0:4600"), registry));

    let _ = app.run();

    let _ = source.stop();
    let _ = sink.map(|mut s| s.stop());
    let _ = decoder.map(|d| d.stop());
    metrics_server.map(|x| x.stop());
    state.map(|s| s.stop());
}


/// AIS Stream
#[derive(Parser)]
#[command(version, about=None, long_about=None)]
struct Cli {

    /// Source of the stream
    ///
    /// Format: {n}
    ///     - 'tcp://IP:PORT'         Stream from TCP server{n}
    ///     - 'file://ABSOLUTE_PATH'  Read from file (*.{nmea,rar}) or file list (*.list)
    source: String,

    /// Ouput selection (use --help to see all the options)
    ///
    /// Options:{n}
    ///     -                          print to STDOUT{n}
    ///     file://ABSOLUTE_PATH       write to file at ABSOLUTE_PATH{n}
    ///     tcp://LISTEN_ADDRESS:PORT  start a TCP server{n}
    ///     http://LISTEN_ADDRESS:PORT start a HTTP server serving SSE{n}
    ///     sqlite://ABSOLUTE_PATH     save to SQLite database{n}
    ///     postgresql://...           save to PostgreSQL database
    sink: Option<String>,

    /// Output format.
    #[arg(long)]
    format: Option<SinkFormat>,

    // /// Enable metrics.
    // #[arg(long)]
    // metrics: Option<String>, // TODO

    /// Geographical grid.
    ///
    /// Path to CSV with one cell per line and columns "id" with integers and "geom" with wkt strings.
    #[arg(long)]
    grid: Option<String>,

    /// Geographical lines.
    #[arg(long)]
    lines: Option<String>,

    /// Store only analysis data in the database.
    #[arg(long, default_value_t = false)]
    analysis_only: bool,

}


struct Config {
    pub source: Source,
    pub sink: Option<Sink>,
    pub sink_format: SinkFormat,
    // pub grid: Option<(String, String)>,
    pub grid: Option<String>,
    pub lines: Option<String>,
    pub analysis_only: bool,
}


impl Config {

    pub fn new(cli: Cli) -> Result<Config, ConfigErr> {
        let source = url::Url::parse(&cli.source)
            .map_err(|e| SourceErr::InvalidURL(e.to_string()))
            .and_then(|url| Source::new(&url));
        let source = match source {
            Err(e) => return Err(ConfigErr::Source(e)),
            Ok(source) => source,
        };
        let sink: Option<Sink> = match cli.sink {
            None => None,
            Some(sink) => Some(
                if sink == "-" {
                    Sink::File { path: None }
                } else {
                    let sink = url::Url::parse(&sink)
                        .map_err(|e| SinkErr::InvalidURL(e.to_string()))
                        .and_then(|url| Sink::from_url(&url));
                    match sink {
                        Err(e) => return Err(ConfigErr::Sink(e)),
                        Ok(sink) => sink,
                    }
                },
            )
        };
        let sink_format = match cli.format {
            None => match sink {
                None => SinkFormat::Raw,
                Some(Sink::File { .. }) => SinkFormat::Raw,
                Some(Sink::Tcp { .. }) => SinkFormat::Raw,
                Some(Sink::Http { .. }) => SinkFormat::Raw,
                Some(Sink::Sqlite { .. }) => SinkFormat::Filtered,
                Some(Sink::Postgresql { .. }) => SinkFormat::Filtered,
            },
            Some(format) => match sink {
                None => {
                    return Err(ConfigErr::SinkFormat(String::from(
                        "cannot set format without a sink",
                    )))
                }
                Some(Sink::File { .. }) | Some(Sink::Tcp { .. }) | Some(Sink::Http { .. }) => {
                    format
                }
                Some(Sink::Sqlite { .. }) => {
                    if format == SinkFormat::Filtered {
                        format
                    } else {
                        return Err(ConfigErr::SinkFormat(String::from(
                            "invalid format for SQLite sink",
                        )));
                    }
                }
                Some(Sink::Postgresql { .. }) => {
                    if format == SinkFormat::Filtered {
                        format
                    } else {
                        return Err(ConfigErr::SinkFormat(String::from(
                            "invalid format for PostgreSQL sink",
                        )));
                    }
                }
            },
        };
        let grid = cli.grid;
        let lines = cli.lines;
        let analysis_only = cli.analysis_only;
        Ok(Config {
            source,
            sink,
            sink_format,
            grid,
            lines,
            analysis_only,
        })
    }

}


enum ConfigErr {
    Source(SourceErr),
    Sink(SinkErr),
    SinkFormat(String),
}


/// AIS Stream data source.
enum Source {
    /// Data is read from a file at the given path.
    File { path: String, },
    /// Data is streamed from a TCP server at the given address.
    Tcp { address: String, },
}


impl Source {

    pub fn new (url: &url::Url) -> Result<Source, SourceErr> {
        match url.scheme() {
            "file" => Ok(Source::File{ path: url.path().to_owned() }),
            "tcp" => {
                match (url.host_str(), url.port()) {
                    (Some(h), Some(p)) => {
                        let mut address = h.to_owned();
                        address.push(':');
                        address.push_str(&p.to_string());
                        Ok(Source::Tcp{ address })
                    },
                    _ => Err(SourceErr::InvalidTCPAddress)
                }
            },
            _ => Err(SourceErr::UnsupportedURL)
        }
    }

}


enum SourceErr {
    InvalidURL(String),
    UnsupportedURL,
    InvalidTCPAddress,
}


/// Output of the pipeline.
enum Sink {
    /// File/stdout.
    File { path: Option<String> },
    /// TCP server.
    Tcp { address: String },
    /// HTTP SSE server
    Http { address: String },
    /// SQLite database.
    Sqlite { path: String },
    /// PostgreSQL database.
    Postgresql { uri: String },
}


impl Sink {
    pub fn from_url(url: &url::Url) -> Result<Sink, SinkErr> {
        match url.scheme() {
            "file" => Ok(Sink::File {
                path: Some(url.path().to_owned()),
            }),
            "tcp" => match (url.host_str(), url.port()) {
                (Some(h), Some(p)) => Ok(Sink::Tcp {
                    address: format!("{h}:{p}"),
                }),
                _ => Err(SinkErr::InvalidTCPAddress),
            },
            "http" => match (url.host_str(), url.port()) {
                (Some(h), Some(p)) => Ok(Sink::Http {
                    address: format!("{h}:{p}"),
                }),
                _ => Err(SinkErr::InvalidHTTPAddress),
            },
            "sqlite" => Ok(Sink::Sqlite {
                path: url.path().to_owned(),
            }),
            "sqlite3" => Ok(Sink::Sqlite {
                path: url.path().to_owned(),
            }),
            "postgres" => Ok(Sink::Postgresql {
                uri: url.to_string(),
            }),
            "postgresql" => Ok(Sink::Postgresql {
                uri: url.to_string(),
            }),
            _ => Err(SinkErr::UnsupportedURL),
        }
    }

}


enum SinkErr {
    InvalidURL(String),
    UnsupportedURL,
    InvalidTCPAddress,
    InvalidHTTPAddress,
}


#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, clap::ValueEnum)]
enum SinkFormat {
    Raw,
    Decoded,
    Filtered,
}


#[derive(Clone)]
struct App {
    stop_condvar: Arc<(Mutex<bool>, Condvar)>,
    pub raw_messages: messages::Messages<String>,
    pub decoded_messages: Option<messages::Messages<Message>>,
    pub filtered_messages: Option<messages::Messages<DBItem>>,
}


impl App {
    pub fn new(sink_format: SinkFormat) -> App {
        let stop_condvar = Arc::new((Mutex::new(false), Condvar::new()));
        // message distribution channels
        let raw_messages: messages::Messages<String> =
            messages::Messages::new("raw".to_owned(), MAX_RAW_MESSAGES);
        let decoded_messages: Option<messages::Messages<Message>> = match sink_format {
            SinkFormat::Raw => None,
            _ => Some(messages::Messages::new(
                "decoded".to_owned(),
                MAX_DECODED_MESSAGES,
            )),
        };
        let filtered_messages: Option<messages::Messages<DBItem>> = match sink_format {
            SinkFormat::Filtered => Some(messages::Messages::new(
                "filtered".to_owned(),
                MAX_FILTERED_MESSAGES,
            )),
            _ => None,
        };
        App {
            stop_condvar,
            raw_messages,
            decoded_messages,
            filtered_messages,
        }
    }


    fn internal_stop(stop_condvar: Arc<(Mutex<bool>, Condvar)>) {
        let (lock, cvar) = &*stop_condvar;
        let mut stop = lock.lock().unwrap();
        *stop = true;
        cvar.notify_one();
    }


    fn wait_stop(&self, signals_handle: signal_hook::iterator::Handle) {
        let (lock, cvar) = &*self.stop_condvar;
        let mut stop = lock.lock().unwrap();
        while !*stop {
            stop = cvar.wait(stop).unwrap();
        }
        signals_handle.close();
    }


    fn wait_messages(&self) -> std::thread::Result<()> {
        let mut raw_messages = self.raw_messages.clone();
        let raw_thread = std::thread::spawn(move || { raw_messages.run_tokio(); });
        let decoded_thread = match self.decoded_messages.as_ref() {
            Some(d) => {
                let mut decoded_messages = d.clone();
                Some(std::thread::spawn(move || { decoded_messages.run_tokio(); }))
            }
            None => None
        };
        let filtered_thread = match self.filtered_messages.as_ref() {
            Some(d) => {
                let mut filtered_messages = d.clone();
                Some(std::thread::spawn(move || { filtered_messages.run_tokio(); }))
            }
            None => None
        };
        let _ = raw_thread.join()?;
        if let Some(dt) = decoded_thread {
            dt.join()?;
        }
        if let Some(ft) = filtered_thread {
            ft.join()?;
        }
        Self::internal_stop(self.stop_condvar.clone());
        Ok(())
    }


    pub fn run(self) -> std::thread::Result<()> {
        let signals = Self::setup_signals();
        let handle = signals.handle();
        let raw_messages_sender = self.raw_messages.sender();
        std::thread::spawn(move || {
            Self::handle_signals(signals, raw_messages_sender);
        });
        let app_clone = self.clone();
        std::thread::spawn(move || {
            let _ = app_clone.wait_messages();
        });
        self.wait_stop(handle);
        Ok(())
    }


    fn setup_signals() -> Signals {
        let force_termination = Arc::new(AtomicBool::new(false));
        for sig in TERM_SIGNALS {
            flag::register_conditional_shutdown(*sig, 1, Arc::clone(&force_termination))
                .expect("forced termination signal setup");
            flag::register(*sig, Arc::clone(&force_termination))
                .expect("termination signal setup");
        }
        Signals::new(TERM_SIGNALS)
            .expect("signals setup")
    }


    fn handle_signals(mut signals: Signals, raw_messages: messages::Sender<String>) {
        if (&mut signals).into_iter().next().is_some() {
            log_info("terminating...");
            let _ = raw_messages.close();
        }
    }

}
