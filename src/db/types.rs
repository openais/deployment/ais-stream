use serde::Serialize;

use crate::lib::ais::{
    state::{GridCellInfo3, GridPosition, ShipInfo, ShipState, ShipVoyage},
    types::MMSI,
};

#[derive(Clone, Serialize)]
#[serde(tag = "type")]
pub enum DBItem {
    Vessel {
        mmsi: MMSI,
        #[serde(flatten)]
        value: ShipInfo,
    },
    Voyage {
        mmsi: MMSI,
        #[serde(flatten)]
        value: ShipVoyage,
    },
    Position {
        mmsi: MMSI,
        #[serde(flatten)]
        value: ShipState,
    },
    GridPosition(GridPosition),
    GridUpdate {
        timestamp: chrono::DateTime<chrono::Utc>,
        grid: Vec<GridCellInfo3>,
    },
    LineIntersection {
        timestamp: chrono::DateTime<chrono::Utc>,
        mmsi: MMSI,
        shiptype: Option<u8>,
        line: i32,
    },
}
