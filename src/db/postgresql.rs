use tokio_postgres as postgres;
use std::sync::{Arc, Mutex};
use futures_util::{pin_mut, SinkExt};

use crate::db::types::DBItem;
use crate::lib::ais::state::{GridCellInfo3, GridPosition, ShipInfo, ShipState, ShipVoyage};
use crate::lib::ais::types::MMSI;
use crate::log::log_error;
use crate::utils::messages;


pub const DB_UPSTREAM_LIMIT: usize = 10000;


#[derive(Clone)]
pub struct PostgreSQLDB {
    conn: Arc<Mutex<postgres::Client>>,
}


pub struct PostgreSQLDBRunner {
    thread: Option<std::thread::JoinHandle<()>>,
}

impl PostgreSQLDBRunner {

    pub fn spawn(connection: &str, upstream: messages::SyncReceiver<DBItem>) -> PostgreSQLDBRunner {

        let connection = String::from(connection);
        let thread = std::thread::spawn(move || {
            PostgreSQLDBRunner::run(&connection, upstream).expect("PostgreSQLDBRunner run");
        });
        PostgreSQLDBRunner {
            thread: Some(thread),
        }
    }

    #[tokio::main(flavor = "current_thread")]
    async fn run(connection: &str, mut upstream: messages::SyncReceiver<DBItem>) -> Result<(), postgres::Error> {
        let (client, conn) = postgres::connect(connection, postgres::NoTls).await?;
        tokio::spawn(async move {
            if let Err(e) = conn.await {
                log_error(format!("postgres connection error: {}", e));
            }
        });
        let conn = Arc::new(Mutex::new(client));
        let db = PostgreSQLDB { conn };
        db.init().await?;
        let mut buf: Vec<DBItem> = Vec::with_capacity(DB_UPSTREAM_LIMIT);
        let mut buf_vessels: Vec<String> = Vec::with_capacity(DB_UPSTREAM_LIMIT);
        let mut buf_voyages: Vec<String> = Vec::with_capacity(DB_UPSTREAM_LIMIT);
        let mut buf_positions: Vec<String> = Vec::with_capacity(DB_UPSTREAM_LIMIT);
        let mut buf_grid_positions: Vec<String> = Vec::with_capacity(DB_UPSTREAM_LIMIT);
        let mut buf_grid_updates: Vec<String> = Vec::with_capacity(DB_UPSTREAM_LIMIT * 10);
        let mut buf_line_intersections: Vec<String> = Vec::with_capacity(DB_UPSTREAM_LIMIT * 10);
        let mut conn = db.conn.lock().unwrap();
        // let mut stop = false;
        // while !stop {
        loop {
            let n = upstream.recv_many(&mut buf, DB_UPSTREAM_LIMIT).await;
            if n == 0 { break }
            for d in buf.iter() {
                match d {
                    DBItem::Vessel {
                        mmsi,
                        value: ship_info,
                    } => buf_vessels.push(Self::buffer_vessel(*mmsi, ship_info)),
                    DBItem::Voyage {
                        mmsi,
                        value: ship_voyage,
                    } => buf_voyages.push(Self::buffer_voyage(*mmsi, ship_voyage)),
                    DBItem::Position {
                        mmsi,
                        value: ship_state,
                    } => {
                        // TODO grid cell count increment
                        // TODO grid cell time sum update
                        // TODO grid cell speed ?
                        buf_positions.push(Self::buffer_position(*mmsi, ship_state))
                    }
                    DBItem::GridPosition(grid_position) => {
                        buf_grid_positions.push(Self::buffer_grid_position(grid_position))
                    }
                    DBItem::GridUpdate {
                        timestamp: ts_bucket,
                        grid: cell_updates,
                    } => buf_grid_updates
                        .append(&mut Self::buffer_grid_updates(ts_bucket, cell_updates)),
                    DBItem::LineIntersection {
                        timestamp: ts_bucket,
                        mmsi,
                        shiptype,
                        line,
                    } => buf_line_intersections.push(Self::buffer_line_intersections(
                        ts_bucket, *mmsi, shiptype, *line,
                    )),
                    // None => { stop = true; },
                };
            }
            let transaction = conn.transaction().await.expect("postgres transaction");
            Self::copy_to_database(&transaction, "vessel", &buf_vessels).await?; buf_vessels.clear();
            Self::copy_to_database(&transaction, "voyage", &buf_voyages).await?; buf_voyages.clear();
            Self::copy_to_database(&transaction, "position", &buf_positions).await?; buf_positions.clear();
            Self::copy_to_database(&transaction, "grid_position", &buf_grid_positions).await?; buf_grid_positions.clear();
            Self::copy_to_database(&transaction, "grid_analysis", &buf_grid_updates).await?; buf_grid_updates.clear();
            Self::copy_to_database(&transaction, "line_intersections", &buf_line_intersections).await?; buf_line_intersections.clear();
            buf.clear();
            transaction.commit().await.expect("transaction commit");
        }
        Ok(())
    }


    async fn copy_to_database(transaction: &postgres::Transaction<'_>, table: &str, lines: &[String]) -> Result<(), postgres::Error> {
        let stmt = format!("COPY {table} FROM STDIN WITH (NULL '')");
        let writer = transaction.copy_in(&stmt).await?;
        pin_mut!(writer);
        for line in lines.iter() {
            writer.feed(bytes::Bytes::from(line.clone())).await?;
        }
        writer.close().await?;
        Ok(())
    }


    fn buffer_vessel(mmsi: MMSI, ship_info: &ShipInfo) -> String {
        format!("{}\t{}\t{}\t{}\t\"{}\"\t\"{}\"\t{}\t{}\t{}\t{}\t{}\t{}\n",
            mmsi,
            ship_info.timestamp.timestamp_millis(),
            ship_info.ais_version.map_or(String::new(), |a| a.to_string()),
            ship_info.imo.map_or(String::new(), |a| a.to_string()),
            ship_info.callsign.clone().unwrap_or_default().replace("\\", "\\\\"),
            ship_info.shipname.replace("\\", "\\\\"),
            ship_info.shiptype.map_or(String::new(), |a| a.to_string()),
            ship_info.to_bow,
            ship_info.to_stern,
            ship_info.to_port,
            ship_info.to_starboard,
            ship_info.epfd,
        )
    }


    fn buffer_voyage(mmsi: MMSI, ship_voyage: &ShipVoyage) -> String {
        format!("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n",
            mmsi,
            ship_voyage.timestamp.timestamp_millis(),
            ship_voyage.draught,
            ship_voyage.destination.replace("\\", "\\\\"),
            ship_voyage.month,
            ship_voyage.day,
            ship_voyage.hour,
            ship_voyage.minute,
        )
    }


    fn buffer_position(mmsi: MMSI, ship_state: &ShipState) -> String {
        format!("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n",
            mmsi,
            ship_state.timestamp.timestamp_millis(),
            ship_state.status.map_or(String::new(), |a| a.to_string()),
            ship_state.turn.map_or(String::new(), |a| a.to_string()),
            ship_state.speed.map_or(String::new(), |a| a.to_string()),
            ship_state.accuracy, // bool,
            ship_state.lon,
            ship_state.lat,
            ship_state.course.map_or(String::new(), |a| a.to_string()),
            ship_state.heading.map_or(String::new(), |a| a.to_string()),
            (ship_state.second as i32),
            ship_state.maneuver.map_or(String::new(), |a| a.to_string()),
            ship_state.raim, // bool,
            ship_state.radio.map_or(String::new(), |a| a.to_string()),
        )
    }


    fn buffer_grid_position(grid_position: &GridPosition) -> String {
        format!("{}\t{}\t{}\t{}\t{}\t{}\t{}\n",
            grid_position.grid,
            grid_position.mmsi,
            grid_position.timestamp.timestamp_millis(),
            grid_position.shiptype.map_or(String::new(), |a| a.to_string()),
            grid_position.width.map_or(String::new(), |a| a.to_string()),
            grid_position.length.map_or(String::new(), |a| a.to_string()),
            grid_position.draught.map_or(String::new(), |a| a.to_string()),
        )
    }


    fn buffer_grid_updates(ts_bucket: &chrono::DateTime<chrono::Utc>, cell_updates: &[GridCellInfo3]) -> Vec<String> {
        cell_updates.iter().map(|cell_info|
            format!("{}\t{}\t{}\t{}\t{}\n",
                ts_bucket.timestamp_millis(),
                cell_info.grid.map_or(String::new(), |a| a.to_string()),
                cell_info.shiptype.map_or(String::new(), |a| a.to_string()),
                cell_info.count,
                cell_info.duration.num_milliseconds(),
            )
        ).collect()
    }


    fn buffer_line_intersections(ts_bucket: &chrono::DateTime<chrono::Utc>, mmsi: MMSI, shiptype: &Option<u8>, line: i32) -> String {
        format!("{}\t{}\t{}\t{}\n",
            ts_bucket.timestamp_millis(),
            mmsi,
            shiptype.map_or(String::new(), |a| a.to_string()),
            line,
        )
    }

}


impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for PostgreSQLDBRunner {

    fn stop(&mut self) -> std::thread::Result<()> {
        match self.thread.take().map(std::thread::JoinHandle::join) {
            None => unreachable!(),
            Some(Err(e)) => Err(e),
            Some(Ok(())) => Ok(()),
        }
    }

}


impl PostgreSQLDB {

    async fn init(&self) -> Result<(), postgres::Error> {
        let conn = self.conn.lock().unwrap();
        conn.batch_execute("
            BEGIN;
            CREATE TABLE IF NOT EXISTS vessel (
                mmsi INTEGER NOT NULL,
                timestamp BIGINT NOT NULL,
                ais_version INTEGER,
                imo INTEGER,
                callsign TEXT,
                shipname TEXT,
                shiptype INTEGER,
                to_bow INTEGER,
                to_stern INTEGER,
                to_port INTEGER,
                to_starboard INTEGER,
                epfd INTEGER
            );
            CREATE TABLE IF NOT EXISTS voyage (
                mmsi INTEGER NOT NULL,
                timestamp BIGINT NOT NULL,
                draught INTEGER NOT NULL,
                destination TEXT,
                month INTEGER,
                day INTEGER,
                hour INTEGER,
                minute INTEGER
            );
            CREATE TABLE IF NOT EXISTS position (
                mmsi INTEGER NOT NULL,
                timestamp BIGINT NOT NULL,
                status INTEGER,
                turn INTEGER,
                speed INTEGER,
                accuracy boolean NOT NULL,
                lon INTEGER,
                lat INTEGER,
                course INTEGER,
                heading INTEGER,
                second INTEGER,
                maneuver INTEGER,
                raim boolean,
                radio INTEGER,
	            geog geography GENERATED ALWAYS AS ( ST_Point(lon/600000.0, lat/600000.0, 4326) ) STORED
            );
            CREATE INDEX IF NOT EXISTS position_geog_index
                ON public.position
                USING GIST (geog);
            CREATE TABLE IF NOT EXISTS grid_position (
                grid INTEGER NOT NULL,
                mmsi INTEGER NOT NULL,
                timestamp BIGINT NOT NULL,
                type INTEGER,
                width INTEGER,
                length INTEGER,
                draught INTEGER
            );
            CREATE TABLE IF NOT EXISTS grid_analysis (
                time_bucket BIGINT NOT NULL,
                grid INTEGER,
                shiptype INTEGER,
                count BIGINT NOT NULL,
                duration BIGINT NOT NULL
            );
            CREATE TABLE IF NOT EXISTS line_intersections (
                time_bucket BIGINT NOT NULL,
                mmsi INTEGER NOT NULL,
                shiptype INTEGER,
                line INTEGER NOT NULL
            );
            COMMIT;
        ").await?;
        Ok(())
    }

}
