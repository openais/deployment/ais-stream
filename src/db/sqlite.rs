use std::{path::Path, sync::{Arc, Mutex}};

use crate::{lib::ais::state::{ShipInfo, ShipState, ShipVoyage, GridPosition}, utils::messages};
use crate::lib::ais::types::MMSI;

use crate::db::types::DBItem;


pub const DB_UPSTREAM_LIMIT: usize = 1000;


#[derive(Clone)]
pub struct SQLiteDB {
    conn: Arc<Mutex<rusqlite::Connection>>,
}


pub struct SQLiteDBRunner {
    thread: Option<std::thread::JoinHandle<()>>,
}

impl SQLiteDBRunner {

    pub fn spawn(db: SQLiteDB, upstream: messages::SyncReceiver<DBItem>) -> SQLiteDBRunner {
        let thread_db = db.clone();
        let thread = std::thread::spawn(move || {
            SQLiteDBRunner::run(thread_db, upstream);
        });
        SQLiteDBRunner {
            thread: Some(thread),
        }
    }

    #[tokio::main(flavor = "current_thread")]
    async fn run(db: SQLiteDB, mut upstream: messages::SyncReceiver<DBItem>) {
        let mut buf: Vec<DBItem> = Vec::with_capacity(DB_UPSTREAM_LIMIT);
        let conn = db.conn.lock().unwrap();
        let mut add_vessel_stmt = conn.prepare("INSERT INTO vessel VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)").expect("add_vessel prepared statement");
        let mut add_voyage_stmt = conn.prepare("INSERT INTO voyage VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)").expect("add_voyage prepared statement");
        let mut add_position_stmt = conn.prepare("INSERT INTO position VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14)").expect("add_position prepared statement");
        let mut add_grid_position_stmt = conn.prepare("INSERT INTO grid_position VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)").expect("add_grid_position prepared statement");
        loop {
            let n = upstream.recv_many(&mut buf, DB_UPSTREAM_LIMIT).await;
            if n == 0 {
                break;
            }
            let _ = conn.execute("BEGIN TRANSACTION", ()); // TODO handle errors
            buf.iter().for_each(|d| {
                let _ = match d {
                    // TODO handle errors
                    DBItem::Vessel { mmsi, value } => {
                        SQLiteDB::add_vessel_prepared(&mut add_vessel_stmt, *mmsi, value)
                    }
                    DBItem::Voyage { mmsi, value } => {
                        SQLiteDB::add_voyage_prepared(&mut add_voyage_stmt, *mmsi, value)
                    }
                    DBItem::Position { mmsi, value } => {
                        SQLiteDB::add_position_prepared(&mut add_position_stmt, *mmsi, value)
                    }
                    DBItem::GridPosition(grid_position) => SQLiteDB::add_grid_position_prepared(
                        &mut add_grid_position_stmt,
                        grid_position,
                    ),
                    DBItem::GridUpdate { .. } => {
                        /* TODO */
                        Ok(())
                    }
                    DBItem::LineIntersection { .. } => {
                        /* TODO */
                        Ok(())
                    }
                };
            });
            buf.clear();
            let _ = conn.execute("COMMIT", ()); // TODO handle errors
        }
    }

}


impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for SQLiteDBRunner {

    fn stop(&mut self) -> std::thread::Result<()> {
        match self.thread.take().map(std::thread::JoinHandle::join) {
            None => unreachable!(),
            Some(Err(e)) => Err(e),
            Some(Ok(())) => Ok(()),
        }
    }

}


impl SQLiteDB {

    pub fn new<P>(path: P) -> rusqlite::Result<SQLiteDB>
    where
        P: AsRef<Path>,
    {
        let conn = Arc::new(Mutex::new(rusqlite::Connection::open(path)?));
        let sqlitedb = SQLiteDB { conn };
        sqlitedb.init()?;
        Ok(sqlitedb)
    }

    fn init(&self) -> rusqlite::Result<()> {
        let conn = self.conn.lock().unwrap();
        conn.execute_batch(
            "
            BEGIN;
            CREATE TABLE IF NOT EXISTS vessel (
                mmsi INTEGER NOT NULL,
                timestamp INTEGER NOT NULL,
                ais_version INTEGER,
                imo INTEGER,
                callsign TEXT,
                shipname TEXT,
                shiptype INTEGER,
                to_bow INTEGER,
                to_stern INTEGER,
                to_port INTEGER,
                to_starboard INTEGER,
                epfd INTEGER
            );
            CREATE TABLE IF NOT EXISTS voyage (
                mmsi INTEGER NOT NULL,
                timestamp INTEGER NOT NULL,
                draught INTEGER NOT NULL,
                destination TEXT,
                month INTEGER,
                day INTEGER,
                hour INTEGER,
                minute INTEGER
            );
            CREATE TABLE IF NOT EXISTS position (
                mmsi INTEGER NOT NULL,
                timestamp INTEGER NOT NULL,
                status INTEGER,
                turn INTEGER,
                speed INTEGER,
                accuracy INTEGER NOT NULL,
                lon INTEGER,
                lat INTEGER,
                course INTEGER,
                heading INTEGER,
                second INTEGER,
                maneuver INTEGER,
                raim INTEGER,
                radio INTEGER
            );
            CREATE TABLE IF NOT EXISTS grid_position (
                grid INTEGER NOT NULL,
                mmsi INTEGER NOT NULL,
                timestamp INTEGER NOT NULL,
                type INTEGER,
                width INTEGER,
                length INTEGER,
                draught INTEGER
            );
            COMMIT;
        ",
        )?;
        Ok(())
    }


    pub fn add_vessel(&self, mmsi: MMSI, ship_info: &ShipInfo) -> rusqlite::Result<()> {
        let conn = self.conn.lock().unwrap();
        conn.execute(
            "INSERT INTO vessel VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12)",
            ( mmsi
            , ship_info.timestamp.timestamp_millis()
            , ship_info.ais_version
            , ship_info.imo
            , &ship_info.callsign
            , &ship_info.shipname
            , ship_info.shiptype
            , ship_info.to_bow
            , ship_info.to_stern
            , ship_info.to_port
            , ship_info.to_starboard
            , ship_info.epfd
            )
        )?;
        Ok(())
    }

    fn add_vessel_prepared(stmt: &mut rusqlite::Statement, mmsi: MMSI, ship_info: &ShipInfo) -> rusqlite::Result<()> {
        stmt.execute(
            ( mmsi
            , ship_info.timestamp.timestamp_millis()
            , ship_info.ais_version
            , ship_info.imo
            , &ship_info.callsign
            , &ship_info.shipname
            , ship_info.shiptype
            , ship_info.to_bow
            , ship_info.to_stern
            , ship_info.to_port
            , ship_info.to_starboard
            , ship_info.epfd
            )
        )?;
        Ok(())
    }


    pub fn add_voyage(&self, mmsi: MMSI, ship_voyage: &ShipVoyage) -> rusqlite::Result<()> {
        let conn = self.conn.lock().unwrap();
        conn.execute(
            "INSERT INTO voyage VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8)",
            ( mmsi
            , ship_voyage.timestamp.timestamp_millis()
            , ship_voyage.draught
            , &ship_voyage.destination
            , ship_voyage.month
            , ship_voyage.day
            , ship_voyage.hour
            , ship_voyage.minute
            )
        )?;
        Ok(())
    }


    fn add_voyage_prepared(stmt: &mut rusqlite::Statement, mmsi: MMSI, ship_voyage: &ShipVoyage) -> rusqlite::Result<()> {
        stmt.execute(
            ( mmsi
            , ship_voyage.timestamp.timestamp_millis()
            , ship_voyage.draught
            , &ship_voyage.destination
            , ship_voyage.month
            , ship_voyage.day
            , ship_voyage.hour
            , ship_voyage.minute
            )
        )?;
        Ok(())
    }


    pub fn add_position(&self, mmsi: MMSI, ship_state: &ShipState) -> rusqlite::Result<()> {
        let conn = self.conn.lock().unwrap();
        conn.execute(
            "INSERT INTO position VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?14)",
            ( mmsi
            , ship_state.timestamp.timestamp_millis()
            , ship_state.status
            , ship_state.turn
            , ship_state.speed
            , ship_state.accuracy
            , ship_state.lon
            , ship_state.lat
            , ship_state.course
            , ship_state.heading
            , ship_state.second
            , ship_state.maneuver
            , ship_state.raim
            , ship_state.radio
            )
        )?;
        Ok(())
    }


    fn add_position_prepared(stmt: &mut rusqlite::Statement, mmsi: MMSI, ship_state: &ShipState) -> rusqlite::Result<()> {
        stmt.execute(
            ( mmsi
            , ship_state.timestamp.timestamp_millis()
            , ship_state.status
            , ship_state.turn
            , ship_state.speed
            , ship_state.accuracy
            , ship_state.lon
            , ship_state.lat
            , ship_state.course
            , ship_state.heading
            , ship_state.second
            , ship_state.maneuver
            , ship_state.raim
            , ship_state.radio
            )
        )?;
        Ok(())
    }


    fn add_grid_position_prepared(stmt: &mut rusqlite::Statement, grid_position: &GridPosition) -> rusqlite::Result<()> {
        stmt.execute(
            ( grid_position.grid
            , grid_position.mmsi
            , grid_position.timestamp.timestamp_millis()
            , grid_position.shiptype
            , grid_position.width
            , grid_position.length
            , grid_position.draught
            )
        )?;
        Ok(())
    }

}
