use std::{path::Path, sync::{Arc, Mutex}};

use crate::lib::ais::state::GridPosition;
use crate::utils::messages;


pub const DB_UPSTREAM_LIMIT: usize = 1000;


#[derive(Clone)]
pub struct GridSQLiteDB {
    conn: Arc<Mutex<rusqlite::Connection>>,
}


pub struct GridSQLiteDBRunner {
    thread: Option<std::thread::JoinHandle<()>>,
}

impl GridSQLiteDBRunner {

    pub fn spawn(db: GridSQLiteDB, upstream: messages::SyncReceiver<GridPosition>) -> GridSQLiteDBRunner {
        let thread_db = db.clone();
        let thread = std::thread::spawn(move || {
            GridSQLiteDBRunner::run(thread_db, upstream);
        });
        GridSQLiteDBRunner {
            thread: Some(thread),
        }
    }


    #[tokio::main(flavor="current_thread")]
    async fn run (db: GridSQLiteDB, mut upstream: messages::SyncReceiver<GridPosition>) {
        let mut buf: Vec<GridPosition> = Vec::with_capacity(DB_UPSTREAM_LIMIT);
        let conn = db.conn.lock().unwrap();
        let mut add_grid_position = conn
            .prepare("INSERT INTO grid_position VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7)")
            .expect("add_grid_position prepared statement");
        loop {
            let n = upstream.recv_many(&mut buf, DB_UPSTREAM_LIMIT).await;
            if n == 0 { break }
            let _ = conn.execute("BEGIN TRANSACTION", ()); // TODO handle errors
            buf.iter().for_each(|gp| {
                let _ = GridSQLiteDB::add_grid_position(&mut add_grid_position, gp);
            });
            buf.clear();
            let _ = conn.execute("COMMIT", ()); // TODO handle errors
        }
    }

}


impl crate::Stop<(), Box<dyn std::any::Any + Send + 'static>> for GridSQLiteDBRunner {

    fn stop(&mut self) -> std::thread::Result<()> {
        match self.thread.take().map(std::thread::JoinHandle::join) {
            None => unreachable!(),
            Some(Err(e)) => Err(e),
            Some(Ok(())) => Ok(()),
        }
    }

}


impl GridSQLiteDB {

    pub fn new<P>(path: P) -> rusqlite::Result<GridSQLiteDB>
    where
        P: AsRef<Path>,
    {
        let conn = Arc::new(Mutex::new(rusqlite::Connection::open(path)?));
        let sqlitedb = GridSQLiteDB {
            conn,
        };
        sqlitedb.init()?;
        Ok(sqlitedb)
    }

    fn init(&self) -> rusqlite::Result<()> {
        let conn = self.conn.lock().unwrap();
        conn.execute_batch("
            BEGIN;
            CREATE TABLE IF NOT EXISTS grid_position (
                grid INTEGER NOT NULL,
                mmsi INTEGER NOT NULL,
                timestamp INTEGER NOT NULL,
                type INTEGER,
                width INTEGER,
                length INTEGER,
                draught INTEGER
            );
            COMMIT;
        ")?;
        Ok(())
    }


    fn add_grid_position(stmt: &mut rusqlite::Statement, grid_position: &GridPosition) -> rusqlite::Result<()> {
        stmt.execute(
            ( grid_position.grid
            , grid_position.mmsi
            , grid_position.timestamp.timestamp_millis()
            , grid_position.shiptype
            , grid_position.width
            , grid_position.length
            , grid_position.draught
            )
        )?;
        Ok(())
    }

}
