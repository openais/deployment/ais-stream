use std::sync::{Arc, Mutex};

use tokio::sync::{broadcast, mpsc};


#[derive(Clone)]
pub struct Messages<T> {
    label: String,
    closed: Arc<Mutex<bool>>,
    upstream_sender: mpsc::Sender<Option<T>>,
    upstream_receiver: Arc<Mutex<mpsc::Receiver<Option<T>>>>,
    downstream_syncs: Arc<Mutex<Vec<mpsc::Sender<T>>>>,
    downstream_asyncs: Arc<Mutex<Option<broadcast::Sender<T>>>>,
}

impl<T> Messages<T>
where
    T: Clone
{

    pub fn new(label: String, async_capacity: usize) -> Messages<T> {
        let closed = Arc::new(Mutex::new(false));
        let (upstream_sender, upstream_receiver) = mpsc::channel(100);
        let upstream_sender = upstream_sender;
        let upstream_receiver = Arc::new(Mutex::new(upstream_receiver));
        let downstream_syncs = Arc::new(Mutex::new(Vec::new()));
        let downstream_asyncs = Arc::new(Mutex::new(Some(broadcast::Sender::new(async_capacity))));
        Messages {
            label,
            closed,
            upstream_sender,
            upstream_receiver,
            downstream_syncs,
            downstream_asyncs,
        }
    }


    pub fn sender(&self) -> Sender<T> {
        Sender {
            label: self.label.clone(),
            sender: self.upstream_sender.clone(),
            closed: self.closed.clone(),
        }
    }


    pub fn subscribe_sync(&mut self, buffer: usize) -> SyncReceiver<T> {
        let (sender, receiver) = mpsc::channel(buffer);
        self.downstream_syncs.lock().unwrap().push(sender);
        SyncReceiver {
            receiver,
        }
    }


    pub fn subscribe_async(&self) -> Option<AsyncReceiver<T>> {
        self.downstream_asyncs.lock().unwrap().as_ref().map( |sender| {
            let receiver = sender.subscribe();
            AsyncReceiver {
                receiver,
            }
    })
    }


    #[tokio::main(flavor="current_thread")]
    pub async fn run_tokio(&mut self) {
        self.run().await;
    }


    pub async fn run(&mut self) {
        loop {
            match self.upstream_receiver.lock().unwrap().recv().await {
                None => {
                    self.close().await;
                    break
                },
                Some(None) => {
                    self.close().await;
                    break
                },
                Some(Some(t)) => {
                    { // send over synchronous channels
                        let mut downstream_syncs = self.downstream_syncs.lock().unwrap();
                        let mut keep: Vec<bool> = Vec::with_capacity(downstream_syncs.len());
                        for ds in downstream_syncs.iter() {
                            keep.push(ds.send(t.clone()).await.is_ok());
                        }
                        let mut i = 0;
                        downstream_syncs.retain(|_| { i+=1; keep[i-1] });
                    }
                    { // send over asynchronous channels
                        let _ = self.downstream_asyncs.lock().unwrap().as_ref().map(|sender| sender.send(t));
                    }
                },
            }
        }
    }


    async fn close(&self) {
        *self.closed.lock().unwrap() = true;
        self.downstream_syncs.lock().unwrap().clear();
        *self.downstream_asyncs.lock().unwrap() = None;
    }

}


pub struct Sender<T> {
    label: String,
    sender: mpsc::Sender<Option<T>>,
    closed: Arc<Mutex<bool>>,
}

impl<T> Sender<T> {

    pub fn send(&self, value: T) -> Result<(), mpsc::error::SendError<T>> {
        if *self.closed.lock().unwrap() {
            return Err(mpsc::error::SendError(value))
        }
        match self.sender.blocking_send(Some(value)) {
            Err(mpsc::error::SendError(None)) => unreachable!(),
            Err(mpsc::error::SendError(Some(v))) => Err(mpsc::error::SendError(v)),
            Ok(()) => Ok(()),
        }
    }


    pub async fn async_send(&self, value: T) -> Result<(), mpsc::error::SendError<T>> {
        if *self.closed.lock().unwrap() {
            return Err(mpsc::error::SendError(value))
        }
        match self.sender.send(Some(value)).await {
            Err(mpsc::error::SendError(None)) => unreachable!(),
            Err(mpsc::error::SendError(Some(v))) => Err(mpsc::error::SendError(v)),
            Ok(()) => Ok(()),
        }
    }


    pub fn close(&self) -> Result<(), mpsc::error::SendError<()>> {
        if *self.closed.lock().unwrap() {
            crate::log::log_error(format!("messages2 sender [{}]: sync closing error", self.label));
            return Err(mpsc::error::SendError(()))
        }
        self.sender.blocking_send(None).or(Err(mpsc::error::SendError(())))
    }


    pub async fn async_close(&self) -> Result<(), mpsc::error::SendError<()>> {
        if *self.closed.lock().unwrap() {
            crate::log::log_error(format!("messages2 sender [{}]: sync closing error", self.label));
            return Err(mpsc::error::SendError(()))
        }
        self.sender.send(None).await.or(Err(mpsc::error::SendError(())))
    }

}


pub struct SyncReceiver<T> {
    receiver: mpsc::Receiver<T>,
}

impl<T> SyncReceiver<T> {

    pub async fn recv(&mut self) -> Option<T> {
        self.receiver.recv().await
    }

    pub async fn recv_many(&mut self, buf: &mut Vec<T>, limit: usize) -> usize {
        self.receiver.recv_many(buf, limit).await
    }

}


pub struct AsyncReceiver<T> {
    receiver: broadcast::Receiver<T>,
}

impl<T> AsyncReceiver<T>
where
    T: Clone
{

    pub async fn recv(&mut self) -> Result<T, broadcast::error::RecvError> {
        self.receiver.recv().await
    }

}
