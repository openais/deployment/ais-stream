use serde_json;

use crate::{db::types::DBItem, lib::ais::types::Message};

pub mod char_array {
    use std::{ffi::CStr, os::raw::c_char};

    use serde::{ser, Serializer};

    pub fn serialize<S, const N: usize>(
        char_array: &[c_char; N],
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let intermediate = unsafe { CStr::from_ptr(char_array.as_ptr()) }
            .to_str()
            .map_err(|e| ser::Error::custom(format!("CStr error: {e}")))?;

        serializer.serialize_str(intermediate)
    }
}

pub mod speed {
    use serde::Serializer;

    const SPEED_INVALID: u16 = 1023;

    pub mod deciknots {
        use super::*;

        pub fn serialize<S>(speed: &u16, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            if *speed == SPEED_INVALID {
                serializer.serialize_f64(*speed as f64)
            } else {
                serializer.serialize_f64((*speed as f64) / 10.0)
            }
        }
    }

    pub mod maybe_deciknots {
        use super::*;

        pub fn serialize<S>(speed: &Option<u16>, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            if let Some(s) = speed {
                deciknots::serialize(s, serializer)
            } else {
                serializer.serialize_none()
            }
        }
    }
}

pub mod course {
    use serde::Serializer;

    const COURSE_INVALID: u16 = 3600;

    pub mod tenth_of_deg {
        use super::*;

        pub fn serialize<S>(course: &u16, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            if *course == COURSE_INVALID {
                serializer.serialize_f64(*course as f64)
            } else {
                serializer.serialize_f64((*course as f64) / 10.0)
            }
        }
    }

    pub mod maybe_tenth_of_deg {
        use super::*;

        pub fn serialize<S>(course: &Option<u16>, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            if let Some(c) = course {
                tenth_of_deg::serialize(c, serializer)
            } else {
                serializer.serialize_none()
            }
        }
    }
}

pub mod heading {
    use serde::Serializer;

    pub fn serialize<S>(heading: &u16, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_u16(*heading)
    }
}

pub mod maybe_heading {
    use super::*;
    use serde::Serializer;

    pub fn serialize<S>(heading: &Option<u16>, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        if let Some(h) = heading {
            heading::serialize(h, serializer)
        } else {
            serializer.serialize_none()
        }
    }
}

pub mod position {
    use serde::Serializer;

    const LON_INVALID: i32 = 0x6791AC0;
    const LAT_INVALID: i32 = 0x3412140;

    const LON_LAT_DIV: f64 = 6f64;
    const LON_LAT_DIV_AFTER_ROUND: f64 = 100000f64;

    pub mod lon_deg {
        use super::*;

        pub fn serialize<S>(lon: &i32, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            if *lon == LON_INVALID {
                serializer.serialize_none()
            } else {
                let lon_deg = ((*lon as f64) / LON_LAT_DIV).round() / LON_LAT_DIV_AFTER_ROUND;
                serializer.serialize_f64(lon_deg)
            }
        }
    }

    pub mod lat_deg {
        use super::*;

        pub fn serialize<S>(lat: &i32, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            if *lat == LAT_INVALID {
                serializer.serialize_none()
            } else {
                let lat_deg = ((*lat as f64) / LON_LAT_DIV).round() / LON_LAT_DIV_AFTER_ROUND;
                serializer.serialize_f64(lat_deg)
            }
        }
    }
}

pub mod delta {
    use chrono::TimeDelta;
    use serde::Serializer;

    pub mod milliseconds {
        use super::*;

        pub fn serialize<S>(delta: &TimeDelta, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            serializer.serialize_i64(delta.num_milliseconds())
        }
    }
}

pub mod tags {
    use chrono::Utc;
    use serde::{ser::SerializeStruct, Serializer};

    use crate::utils::conversion;

    pub fn serialize<S>(tags: &[(Vec<u8>, Vec<u8>)], serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let timestamp = tags
            .iter()
            .find_map(|(t, v)| if t == "c".as_bytes() { Some(v) } else { None })
            .map_or(
                Utc::now(),
                |ts_bytes| match conversion::bytes_to_datetime_utc(ts_bytes) {
                    Ok(dt) => dt,
                    Err(_) => Utc::now(),
                },
            );

        let mut tag_ser = serializer.serialize_struct("tags", 1)?;
        tag_ser.serialize_field("timestamp", &timestamp)?;
        tag_ser.end()
    }
}

pub mod transform {
    use serde::Serializer;

    pub mod number_to_str {
        use super::*;

        pub fn serialize<S, T>(value: &T, serializer: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
            T: std::string::ToString,
        {
            serializer.serialize_str(&value.to_string())
        }
    }
}

impl TryFrom<Message> for Vec<u8> {
    type Error = serde_json::Error;

    fn try_from(value: Message) -> Result<Self, Self::Error> {
        Ok(serde_json::to_string(&value)?.into())
    }
}

impl TryFrom<DBItem> for Vec<u8> {
    type Error = serde_json::Error;

    fn try_from(value: DBItem) -> Result<Self, Self::Error> {
        Ok(serde_json::to_string(&value)?.into())
    }
}

#[cfg(test)]
mod tests {
    use crate::lib::ais::types::{AISMessage, Message, Type24Extra, Type24Part};

    #[test]
    fn test_ais_type1_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type1_2_3 {
                message_type: 1,
                status: 0,
                turn: 0,
                speed: 77,
                accuracy: true,
                lon: 3110024,
                lat: 36229008,
                course: 546,
                heading: 54,
                second: 13,
                maneuver: 0,
                raim: false,
                radio: 82041,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "1",
            "status": 0,
            "turn": 0,
            "speed": 7.7,
            "accuracy": true,
            "lon": 5.18337,
            "lat": 60.38168,
            "course": 54.6,
            "heading": 54,
            "second": 13,
            "maneuver": 0,
            "raim": false,
            "radio": 82041
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type1_with_invalid_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type1_2_3 {
                message_type: 2,
                status: 0,
                turn: 0,
                speed: 1023,
                accuracy: true,
                lon: 0x6791AC0,
                lat: 0x3412140,
                course: 3600,
                heading: 511,
                second: 13,
                maneuver: 0,
                raim: false,
                radio: 82041,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "2",
            "status": 0,
            "turn": 0,
            "speed": 1023.0,
            "accuracy": true,
            "lon": null,
            "lat": null,
            "course": 3600.0,
            "heading": 511,
            "second": 13,
            "maneuver": 0,
            "raim": false,
            "radio": 82041
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type5_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type5 {
                ais_version: 1,
                imo: 0,
                callsign: [76, 70, 53, 50, 57, 55, 0, 0],
                shipname: [
                    82, 69, 83, 67, 85, 69, 32, 66, 69, 82, 71, 69, 83, 69, 78, 32, 68, 46, 89, 46,
                    0,
                ],
                shiptype: 54,
                to_bow: 9,
                to_stern: 6,
                to_port: 3,
                to_starboard: 1,
                epfd: 1,
                month: 3,
                day: 21,
                hour: 12,
                minute: 34,
                draught: 0,
                destination: [
                    84, 76, 70, 32, 48, 50, 48, 49, 54, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                ],
                dte: false,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "5",
            "ais_version": 1,
            "imo": 0,
            "callsign": "LF5297",
            "shipname": "RESCUE BERGESEN D.Y.",
            "shiptype": 54,
            "to_bow": 9,
            "to_stern": 6,
            "to_port": 3,
            "to_starboard": 1,
            "epfd": 1,
            "month": 3,
            "day": 21,
            "hour": 12,
            "minute": 34,
            "draught": 0,
            "destination": "TLF 02016",
            "dte": false,
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type18_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type18 {
                reserved: 0,
                speed: 77,
                accuracy: true,
                lon: 3110024,
                lat: 36229008,
                course: 546,
                heading: 54,
                second: 13,
                regional: 0,
                cs: true,
                display: false,
                dsc: true,
                band: true,
                msg22: true,
                assigned: false,
                raim: false,
                radio: 82041,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "18",
            "reserved": 0,
            "speed": 7.7,
            "accuracy": true,
            "lon": 5.18337,
            "lat": 60.38168,
            "course": 54.6,
            "heading": 54,
            "second": 13,
            "regional": 0,
            "cs": true,
            "display": false,
            "dsc": true,
            "band": true,
            "msg22": true,
            "assigned": false,
            "raim": false,
            "radio": 82041
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type18_with_invalid_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type18 {
                reserved: 0,
                speed: 1023,
                accuracy: true,
                lon: 0x6791AC0,
                lat: 0x3412140,
                course: 3600,
                heading: 511,
                second: 13,
                regional: 0,
                cs: true,
                display: false,
                dsc: true,
                band: true,
                msg22: true,
                assigned: false,
                raim: false,
                radio: 82041,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "18",
            "reserved": 0,
            "speed": 1023.0,
            "accuracy": true,
            "lon": null,
            "lat": null,
            "course": 3600.0,
            "heading": 511,
            "second": 13,
            "regional": 0,
            "cs": true,
            "display": false,
            "dsc": true,
            "band": true,
            "msg22": true,
            "assigned": false,
            "raim": false,
            "radio": 82041
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type19_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type19 {
                reserved: 0,
                speed: 77,
                accuracy: true,
                lon: 3110024,
                lat: 36229008,
                course: 546,
                heading: 54,
                second: 13,
                regional: 0,
                shipname: [
                    82, 69, 83, 67, 85, 69, 32, 66, 69, 82, 71, 69, 83, 69, 78, 32, 68, 46, 89, 46,
                    0,
                ],
                shiptype: 54,
                to_bow: 9,
                to_stern: 6,
                to_port: 3,
                to_starboard: 1,
                epfd: 1,
                raim: false,
                dte: false,
                assigned: false,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "19",
            "reserved": 0,
            "speed": 7.7,
            "accuracy": true,
            "lon": 5.18337,
            "lat": 60.38168,
            "course": 54.6,
            "heading": 54,
            "second": 13,
            "regional": 0,
            "shipname": "RESCUE BERGESEN D.Y.",
            "shiptype": 54,
            "to_bow": 9,
            "to_stern": 6,
            "to_port": 3,
            "to_starboard": 1,
            "epfd": 1,
            "raim": false,
            "dte": false,
            "assigned": false,
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type19_with_invalid_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type19 {
                reserved: 0,
                speed: 1023,
                accuracy: true,
                lon: 0x6791AC0,
                lat: 0x3412140,
                course: 3600,
                heading: 511,
                second: 13,
                regional: 0,
                shipname: [
                    82, 69, 83, 67, 85, 69, 32, 66, 69, 82, 71, 69, 83, 69, 78, 32, 68, 46, 89, 46,
                    0,
                ],
                shiptype: 54,
                to_bow: 9,
                to_stern: 6,
                to_port: 3,
                to_starboard: 1,
                epfd: 1,
                raim: false,
                dte: false,
                assigned: false,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "19",
            "reserved": 0,
            "speed": 1023.0,
            "accuracy": true,
            "lon": null,
            "lat": null,
            "course": 3600.0,
            "heading": 511,
            "second": 13,
            "regional": 0,
            "shipname": "RESCUE BERGESEN D.Y.",
            "shiptype": 54,
            "to_bow": 9,
            "to_stern": 6,
            "to_port": 3,
            "to_starboard": 1,
            "epfd": 1,
            "raim": false,
            "dte": false,
            "assigned": false,
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type21_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type21 {
                aid_type: 0,
                name: [
                    82, 69, 83, 67, 85, 69, 32, 66, 69, 82, 71, 69, 83, 69, 78, 32, 68, 46, 89, 46,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                ],
                accuracy: true,
                lon: 3110024,
                lat: 36229008,
                to_bow: 9,
                to_stern: 6,
                to_port: 3,
                to_starboard: 1,
                epfd: 1,
                second: 13,
                off_position: false,
                regional: 0,
                raim: false,
                virtual_aid: false,
                assigned: false,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "21",
            "aid_type": 0,
            "name": "RESCUE BERGESEN D.Y.",
            "accuracy": true,
            "lon": 5.18337,
            "lat": 60.38168,
            "to_bow": 9,
            "to_stern": 6,
            "to_port": 3,
            "to_starboard": 1,
            "epfd": 1,
            "second": 13,
            "off_position": false,
            "regional": 0,
            "raim": false,
            "virtual_aid": false,
            "assigned": false,
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type21_with_invalid_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type21 {
                aid_type: 0,
                name: [
                    82, 69, 83, 67, 85, 69, 32, 66, 69, 82, 71, 69, 83, 69, 78, 32, 68, 46, 89, 46,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                ],
                accuracy: true,
                lon: 0x6791AC0,
                lat: 0x3412140,
                to_bow: 9,
                to_stern: 6,
                to_port: 3,
                to_starboard: 1,
                epfd: 1,
                second: 13,
                off_position: false,
                regional: 0,
                raim: false,
                virtual_aid: false,
                assigned: false,
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "21",
            "aid_type": 0,
            "name": "RESCUE BERGESEN D.Y.",
            "accuracy": true,
            "lon": null,
            "lat": null,
            "to_bow": 9,
            "to_stern": 6,
            "to_port": 3,
            "to_starboard": 1,
            "epfd": 1,
            "second": 13,
            "off_position": false,
            "regional": 0,
            "raim": false,
            "virtual_aid": false,
            "assigned": false,
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type24_with_main_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type24 {
                part: Type24Part::PartA,
                shipname: [
                    82, 69, 83, 67, 85, 69, 32, 66, 69, 82, 71, 69, 83, 69, 78, 32, 68, 46, 89, 46,
                    0,
                ],
                shiptype: 54,
                vendorid: [82, 83, 0, 0, 0, 0, 0, 0],
                model: 0,
                serial: 33440,
                callsign: [76, 70, 53, 50, 57, 55, 0, 0],
                extra: Type24Extra::Main {
                    to_bow: 26,
                    to_stern: 1,
                    to_port: 1,
                    to_starboard: 20,
                },
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "24",
            "part": "PartA",
            "shipname": "RESCUE BERGESEN D.Y.",
            "shiptype": 54,
            "vendorid": "RS",
            "model": 0,
            "serial": 33440,
            "callsign": "LF5297",
            "extra": {
                "Main": {
                    "to_bow": 26,
                    "to_stern": 1,
                    "to_port": 1,
                    "to_starboard": 20
                }
            }
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }

    #[test]
    fn test_ais_type24_with_auxiliary_to_json() -> anyhow::Result<()> {
        let type1_msg = Message {
            tag: vec![(
                vec![b'c'],
                vec![b'1', b'7', b'3', b'7', b'0', b'3', b'9', b'1', b'3', b'4'],
            )],
            repeat: 0,
            mmsi: 13456,
            message: AISMessage::Type24 {
                part: Type24Part::PartA,
                shipname: [
                    82, 69, 83, 67, 85, 69, 32, 66, 69, 82, 71, 69, 83, 69, 78, 32, 68, 46, 89, 46,
                    0,
                ],
                shiptype: 54,
                vendorid: [82, 83, 0, 0, 0, 0, 0, 0],
                model: 0,
                serial: 33440,
                callsign: [76, 70, 53, 50, 57, 55, 0, 0],
                extra: Type24Extra::Auxiliary {
                    mothership_mmsi: 65431,
                },
            },
        };
        let reference_json = serde_json::json!({
            "timestamp": "2025-01-16T14:52:14Z",
            "mmsi": 13456,
            "type": "24",
            "part": "PartA",
            "shipname": "RESCUE BERGESEN D.Y.",
            "shiptype": 54,
            "vendorid": "RS",
            "model": 0,
            "serial": 33440,
            "callsign": "LF5297",
            "extra": {
                "Auxiliary": {
                    "mothership_mmsi": 65431
                }
            }
        });
        let json = serde_json::to_value(&type1_msg)?;

        assert_eq!(json, reference_json);

        Ok(())
    }
}
