use chrono::{DateTime, Utc};

use thiserror::Error;

const EPOCH_S_LOW_BOUND: i64 = -2e10 as i64;
const EPOCH_S_HIGH_BOUND: i64 = 2e10 as i64;

#[derive(Error, Debug)]
pub enum ConversionError {
    #[error(transparent)]
    Utf8Error(#[from] std::str::Utf8Error),
    #[error(transparent)]
    ParseIntError(#[from] std::num::ParseIntError),
    #[error("Invalid epoch detected: {0}")]
    InvalidEpochError(String),
}

pub fn bytes_to_datetime_utc(data: &[u8]) -> Result<DateTime<Utc>, ConversionError> {
    let epoch = std::str::from_utf8(data)?.parse::<i64>()?;
    if (EPOCH_S_LOW_BOUND..=EPOCH_S_HIGH_BOUND).contains(&epoch) {
        chrono::DateTime::from_timestamp(epoch, 0).ok_or(ConversionError::InvalidEpochError(
            format!("unable to create datetime from {} seconds", epoch),
        ))
    } else {
        chrono::DateTime::from_timestamp_millis(epoch).ok_or(ConversionError::InvalidEpochError(
            format!("unable to create datetime from {} milliseconds", epoch),
        ))
    }
}
