use prometheus_client::encoding::EncodeLabelSet;
use prometheus_client::metrics::{counter::Counter, family::Family};
use prometheus_client::registry::Registry;
use std::sync::Arc;
use tokio::select;
use tokio::sync::Notify;

use crate::lib;
use crate::lib::ais::types::Message;
use crate::log::{log_info, log_warning};
use crate::utils::messages;



pub struct Decoder {
    metrics: DecoderMetrics,
    thread: std::thread::JoinHandle<()>,
    stop: Arc<Notify>,
}

impl Decoder {

    pub fn spawn(upstream: messages::SyncReceiver<String>, downstream: messages::Sender<Message>) -> Decoder {
        let metrics = DecoderMetrics::new();
        let stop = Arc::new(Notify::new());

        let thread_metrics = metrics.clone();
        let thread_stop = stop.clone();
        let thread = std::thread::spawn(move || { Decoder::run_tokio(upstream, downstream, thread_metrics, thread_stop); });

        Decoder {
            metrics,
            thread,
            stop,
        }
    }


    pub fn stop(self) {
        self.stop.notify_one();
        self.thread.join().expect("thread should join");
    }


    pub fn register_metrics(&self, registry: &mut Registry) {
        self.metrics.register(registry)
    }


    #[tokio::main(flavor="current_thread")]
    async fn run_tokio(upstream: messages::SyncReceiver<String>, downstream: messages::Sender<Message>, metrics: DecoderMetrics, stop: Arc<Notify>) {
        loop {
            select! {
                biased;
                _ = stop.notified() => break,
                _ = Self::run(upstream, &downstream, metrics) => break,
            }
        }
        let _ = downstream.async_close().await;
    }


    async fn run(mut upstream: messages::SyncReceiver<String>, downstream: &messages::Sender<Message>, metrics: DecoderMetrics) {
        log_info("decoder: started");
        let mut packet = lib::ais::c::packet_t::new();
        let mut decoder = lib::ais::decoder::new();

        loop {
            match upstream.recv().await {
                None => break,
                Some(line) => {
                    metrics.raw_lines.inc();
                    let line = line.trim_end();
                    if !line.is_empty() {
                        match lib::ais::parser::parse(line.as_bytes()) {
                            Err(e) => {
                                log_warning(format!("decoder: parse error: {e}\n\tline [{}]: {line}", metrics.raw_lines.get() - 1));
                            },
                            Ok(p) => {
                                metrics.parsed_lines.inc();
                                lib::ais::decoder::set_packet(&p, &mut packet);
                                match lib::ais::decoder::decode(&mut decoder.contexts, &mut decoder.ais, &p.tag, &mut packet) {
                                    Err(e) => {
                                        log_warning(format!("decoder: decode error: {e:?}\n\tline: {line}"));
                                    }
                                    Ok(None) => {}
                                    Ok(Some(result)) => {
                                        metrics.decoded_lines.get_or_create(&MessageType{ mtype: result.message_type() }).inc();
                                        // log_info(format!("decoder: type {} MMSI {}", result.message_type(), result.mmsi));
                                        let _ = downstream.async_send(result).await;
                                    },
                                }
                            },
                        }
                    }
                },
            }
        }
        log_info("decoder: ended");
    }

}


struct DecoderMetrics {
    raw_lines: Counter,
    parsed_lines: Counter,
    decoded_lines: Family<MessageType, Counter>,
}


impl DecoderMetrics {

    fn new() -> DecoderMetrics {
        DecoderMetrics {
            raw_lines: Counter::default(),
            parsed_lines: Counter::default(),
            decoded_lines: Family::default(),
        }
    }


    fn clone(&self) -> DecoderMetrics {
        DecoderMetrics {
            raw_lines: self.raw_lines.clone(),
            parsed_lines: self.parsed_lines.clone(),
            decoded_lines: self.decoded_lines.clone(),
        }
    }


    fn register(&self, registry: &mut Registry) {
        registry.register(
            "raw_lines",
            "Number of raw lines to decode",
            self.raw_lines.clone(),
        );
        registry.register(
            "parsed_lines",
            "Number of parsed lines",
            self.parsed_lines.clone(),
        );
        registry.register(
            "decoded_lines",
            "Number of decoded lines per message type",
            self.decoded_lines.clone(),
        );
    }

}

#[derive(Clone, Debug, Hash, PartialEq, Eq, EncodeLabelSet)]
struct MessageType {
    mtype: u8,
}
