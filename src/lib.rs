//! A client/server that connects to an AIS TCP stream and that broadcasts the
//! received messages to any client that connects to it.
pub mod db {
    pub mod grid;
    pub mod postgresql;
    pub mod sqlite;
    pub mod types;
}
pub mod decoder;
pub mod file;
pub mod http {
    pub mod event;
    pub mod handler;
    pub mod metrics;
    pub mod server;
    pub mod stream;
}
pub mod lib {
    pub mod ais;
    pub mod geo;
}
pub mod log;
pub mod state;
pub mod tcp {
    pub mod client;
    pub mod server;
}
pub mod utils {
    pub mod conversion;
    pub mod messages;
    pub mod serialisation;
}

pub trait Stop<T, E = ()> {
    fn stop(&mut self) -> Result<T, E>;
}
