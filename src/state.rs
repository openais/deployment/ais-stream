use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;
use prometheus_client::encoding::{EncodeLabelSet, EncodeLabelValue};
use prometheus_client::metrics::counter::Counter;
use prometheus_client::metrics::family::Family;
use prometheus_client::registry::Registry;
use rstar::RTree;

use crate::db::types::DBItem;
use crate::lib::ais::types::{AISMessage, Type24Part/* , MMSI */};
use crate::lib::ais::types::Message;
use crate::lib::ais::state::{self, GridCellInfo3, /* GridPosition,  */Ship, ShipInfo, ShipState, ShipVoyage};
use crate::lib::geo::GridCell;
use crate::log::{log_error, log_warning};
use crate::utils::{conversion, messages};

pub struct State {
    pub state: Arc<Mutex<state::State>>,
    metrics: StateMetrics,
    thread: JoinHandle<()>,
}


impl State {

    pub fn spawn(grid: Option<RTree<GridCell>>, lines: Option<Vec<crate::lib::geo::Line>>, analysis_only: bool, upstream: messages::SyncReceiver<Message>, downstream: messages::Sender<DBItem>, timestamp: chrono::DateTime<chrono::Utc>) -> State {
        let s = Arc::new(Mutex::new(state::new(timestamp)));
        let metrics = StateMetrics::new();
        let thread_state = s.clone();
        let thread_metrics = metrics.clone();
        let thread = std::thread::spawn(move || Self::run(grid, lines, analysis_only, thread_state, upstream, downstream, thread_metrics) );
        State {
            state: s,
            metrics,
            thread,
        }
    }

    pub fn stop (self) {
        let _ = self.thread.join();
    }


    pub fn register_metrics(&self, registry: &mut Registry) {
        self.metrics.register(registry)
    }


    #[tokio::main(flavor="current_thread")]
    async fn run (
        grid: Option<RTree<GridCell>>,
        lines: Option<Vec<crate::lib::geo::Line>>,
        analysis_only: bool,
        state: Arc<Mutex<state::State>>,
        mut upstream: messages::SyncReceiver<Message>,
        downstream: messages::Sender<DBItem>,
        _metrics: StateMetrics, // TODO Provide metrics for the state.
    ) {
        loop {
            match upstream.recv().await {
                None => {
                    let _ = downstream.async_close().await;
                    break
                },
                Some(Message { tag, mmsi, message, .. } ) => {
                    // let timestamp = tag.iter().map(|(a,b)| if a == ['a'] { return });
                    let timestamp = tag.iter().find_map(|(a,b)| if a == "c".as_bytes() { Some(b) } else { None });
                    let timestamp = match timestamp {
                        None => chrono::Utc::now(),
                        Some(value) => {
                            if !value.iter().all(|&b| b.is_ascii_digit()) {
                                log_warning("state: invalid tag timestamp");
                            }
                            // As per the spec, tag can be in milliseconds or seconds since epoch
                            // Assume if we have a time more than X years in the past or future we
                            // should use seconds iso milliseconds
                            match conversion::bytes_to_datetime_utc(value) {
                                Ok(ts) => ts,
                                Err(e) => {
                                    log_warning(format!("state: invalid tag timestanp: {e:?}"));
                                    chrono::Utc::now()
                                }
                            }
                        }
                    };
                    match message {
                        AISMessage::Type1_2_3 {
                            // message_type,
                            status,
                            turn,
                            speed,
                            accuracy,
                            lon,
                            lat,
                            course,
                            heading,
                            second,
                            maneuver,
                            raim,
                            radio,
                            ..
                        } => {
                            let value = ShipState {
                                timestamp,
                                status: Some(status),
                                turn: Some(turn),
                                speed: Some(speed),
                                accuracy,
                                lon,
                                lat,
                                course: Some(course),
                                heading: Some(heading),
                                second,
                                maneuver: Some(maneuver),
                                raim,
                                radio: Some(radio),
                            };
                            let op = state.lock().unwrap().ship_position(&grid, &lines, mmsi, &value);
                            match op {
                                (crate::lib::ais::state::Operation::Add, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::Update, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::NoOp, ts_bucket, grid_update, intersected_lines) => {
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                            };
                        },
                        AISMessage::Type5 {
                            ais_version,
                            imo,
                            callsign,
                            shipname,
                            shiptype,
                            to_bow,
                            to_stern,
                            to_port,
                            to_starboard,
                            epfd,
                            month,
                            day,
                            hour,
                            minute,
                            draught,
                            destination,
                            // dte,
                            ..
                        } => {
                            let value = ShipInfo {
                                timestamp,
                                ts_update: timestamp,
                                ais_version: Some(ais_version),
                                imo: Some(imo),
                                callsign: Some(State::bytes_to_string(&callsign)),
                                shipname: State::bytes_to_string(&shipname),
                                shiptype: Some(shiptype),
                                to_bow,
                                to_stern,
                                to_port,
                                to_starboard,
                                epfd,
                            };
                            let op = state.lock().unwrap().ship(Ship::set_info, mmsi, &value);
                            if !analysis_only {
                                let _ = match op {
                                    crate::lib::ais::state::Operation::Add => downstream.async_send(DBItem::Vessel{mmsi, value}).await,
                                    crate::lib::ais::state::Operation::Update => { downstream.async_send(DBItem::Vessel{mmsi, value}).await },
                                    crate::lib::ais::state::Operation::NoOp => Ok(()),
                                };
                            }
                            let value = ShipVoyage {
                                timestamp,
                                draught,
                                destination: State::bytes_to_string(&destination),
                                month,
                                day,
                                hour,
                                minute,
                            };
                            let op = state.lock().unwrap().ship(Ship::set_voyage, mmsi, &value);
                            if !analysis_only {
                                let _ = match op {
                                    crate::lib::ais::state::Operation::Add => downstream.async_send(DBItem::Voyage{mmsi, value}).await,
                                    crate::lib::ais::state::Operation::Update => { downstream.async_send(DBItem::Voyage{mmsi, value}).await },
                                    crate::lib::ais::state::Operation::NoOp => Ok(()),
                                };
                            }
                        },
                        AISMessage::Type18 {
                            // reserved,
                            speed,
                            accuracy,
                            lon,
                            lat,
                            course,
                            heading,
                            second,
                            // regional,
                            // cs,
                            // display,
                            // dsc,
                            // band,
                            // msg22,
                            // assigned,
                            raim,
                            radio,
                            ..
                        } => {
                            let value = ShipState {
                                timestamp,
                                status: None,
                                turn: None,
                                speed: Some(speed),
                                accuracy,
                                lon,
                                lat,
                                course: Some(course),
                                heading: Some(heading),
                                second,
                                maneuver: None,
                                raim,
                                radio: Some(radio),
                            };
                            let op = state.lock().unwrap().ship_position(&grid, &lines, mmsi, &value);
                            match op {
                                (crate::lib::ais::state::Operation::Add, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::Update, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::NoOp, ts_bucket, grid_update, intersected_lines) => {
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                            };
                        },
                        AISMessage::Type19 {
                            // reserved,
                            speed,
                            accuracy,
                            lon,
                            lat,
                            course,
                            heading,
                            second,
                            // regional,
                            shipname,
                            shiptype,
                            to_bow,
                            to_stern,
                            to_port,
                            to_starboard,
                            epfd,
                            raim,
                            // dte,
                            // assigned,
                            ..
                        } => {
                            let value = ShipState {
                                timestamp,
                                status: None,
                                turn: None,
                                speed: Some(speed),
                                accuracy,
                                lon,
                                lat,
                                course: Some(course),
                                heading: Some(heading),
                                second,
                                maneuver: None,
                                raim,
                                radio: None,
                            };
                            let op = state.lock().unwrap().ship_position(&grid, &lines, mmsi, &value);

                            match op {
                                (crate::lib::ais::state::Operation::Add, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::Update, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::NoOp, ts_bucket, grid_update, intersected_lines) => {
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                            };

                            let value = ShipInfo {
                                timestamp,
                                ts_update: timestamp,
                                ais_version: None,
                                imo: None,
                                callsign: None,
                                shipname: State::bytes_to_string(&shipname),
                                shiptype: Some(shiptype),
                                to_bow,
                                to_stern,
                                to_port,
                                to_starboard,
                                epfd,
                            };
                            let op = state.lock().unwrap().ship(Ship::set_info, mmsi, &value);
                            if !analysis_only {
                                let _ = match op {
                                    crate::lib::ais::state::Operation::Add => downstream.async_send(DBItem::Vessel{mmsi, value}).await,
                                    crate::lib::ais::state::Operation::Update => { downstream.async_send(DBItem::Vessel{mmsi, value}).await },
                                    crate::lib::ais::state::Operation::NoOp => Ok(()),
                                };
                            }
                        },
                        AISMessage::Type21 {
                            // aid_type,
                            name,
                            accuracy,
                            lon,
                            lat,
                            to_bow,
                            to_stern,
                            to_port,
                            to_starboard,
                            epfd,
                            second,
                            // off_position,
                            // regional,
                            raim,
                            // virtual_aid,
                            // assigned,
                            ..
                        } => {
                            let value = ShipState {
                                timestamp,
                                status: None,
                                turn: None,
                                speed: None,
                                accuracy,
                                lon,
                                lat,
                                course: None,
                                heading: None,
                                second,
                                maneuver: None,
                                raim,
                                radio: None,
                            };
                            let op = state.lock().unwrap().ship_position(&grid, &lines, mmsi, &value);
                            match op {
                                (crate::lib::ais::state::Operation::Add, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::Update, ts_bucket, grid_update, intersected_lines) => {
                                    if !analysis_only { let _ = downstream.async_send(DBItem::Position{mmsi, value}).await; } // TODO Handle errors
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                                (crate::lib::ais::state::Operation::NoOp, ts_bucket, grid_update, intersected_lines) => {
                                    Self::send_grid_bucket(&downstream, grid_update).await;
                                    Self::send_intersected_lines(&downstream, ts_bucket, mmsi, intersected_lines.0, intersected_lines.1).await;
                                },
                            };
                            let value = ShipInfo {
                                timestamp,
                                ts_update: timestamp,
                                ais_version: None,
                                imo: None,
                                callsign: None,
                                shipname: State::bytes_to_string(&name),
                                shiptype: None,
                                to_bow,
                                to_stern,
                                to_port,
                                to_starboard,
                                epfd,
                            };
                            let op = state.lock().unwrap().ship(Ship::set_info, mmsi, &value);
                            if !analysis_only {
                                let _ = match op {
                                    crate::lib::ais::state::Operation::Add => downstream.async_send(DBItem::Vessel{mmsi, value}).await,
                                    crate::lib::ais::state::Operation::Update => { downstream.async_send(DBItem::Vessel{mmsi, value}).await },
                                    crate::lib::ais::state::Operation::NoOp => Ok(()),
                                };
                            }
                        },
                        AISMessage::Type24 {
                            part,
                            // part A
                            shipname,
                            // part B
                            shiptype,
                            // vendorid,
                            // model,
                            // serial,
                            callsign,
                            // extra,
                            ..
                        } => {
                            // TODO create set_ship_name and set_ship_type
                            // functions to avoid overwriting data by setting
                            // only the fields that were provided.
                            let value = ShipInfo {
                                timestamp,
                                ts_update: timestamp,
                                ais_version: None,
                                imo: None,
                                callsign: if part != Type24Part::PartA { Some(State::bytes_to_string(&callsign)) } else { None },
                                shipname: if part == Type24Part::PartA { State::bytes_to_string(&shipname) } else { String::from("") },
                                shiptype: if part != Type24Part::PartA { Some(shiptype) } else { None },
                                to_bow: 0,
                                to_stern: 0,
                                to_port: 0,
                                to_starboard: 0,
                                epfd: 0,
                            };
                            let op = state.lock().unwrap().ship(Ship::set_info, mmsi, &value);
                            if !analysis_only {
                                let _ = match op {
                                    crate::lib::ais::state::Operation::Add => downstream.async_send(DBItem::Vessel{mmsi, value}).await,
                                    crate::lib::ais::state::Operation::Update => { downstream.async_send(DBItem::Vessel{mmsi, value}).await },
                                    crate::lib::ais::state::Operation::NoOp => Ok(()),
                                };
                            }
                        },
                        _ => { /* not supported */ },
                    }
                },
            }
        }
    }


    async fn send_grid_bucket(downstream: &messages::Sender<DBItem>, grid_update: Option<(chrono::DateTime<chrono::Utc>, Vec<GridCellInfo3>)>) {
        match grid_update {
            None => {},
            Some((ts_bucket, cell_updates)) => {
                let _ = downstream.async_send(DBItem::GridUpdate{timestamp: ts_bucket, grid: cell_updates}).await;
            },
        }
    }


    async fn send_intersected_lines(downstream: &messages::Sender<DBItem>, ts_bucket: chrono::DateTime<chrono::Utc>, mmsi: crate::lib::ais::types::MMSI, shiptype: Option<u8>, intersected_lines: Vec<i32>) {
        if !intersected_lines.is_empty() {
            for line in intersected_lines {
                match downstream.async_send(DBItem::LineIntersection{timestamp: ts_bucket, mmsi, shiptype, line}).await {
                    Ok(_) => continue,
                    Err(_) => {
                        log_warning("send_intersected_lines: downstream is closed");
                        break
                    },
                }
            }
        }
    }


    fn bytes_to_string (b: &[i8]) -> String {
        match String::from_utf8(b.iter().take_while(|&c| *c != 0).map(|&c| c as u8).collect()) {
            Err(e) => {
                log_error(format!("bytes_to_string: {e}: {b:?}"));
                String::new() // TODO
            },
            Ok(s) => s,
        }
    }

}


#[derive(Clone)]
struct StateMetrics {
    vessel_info: Family<ItemLabel, Counter>,
    vessel_voyage: Family<ItemLabel, Counter>,
    vessel_state: Family<VesselStateLabel, Counter>,
    grid_position_in: Counter,
    grid_position_out: Counter,
}


impl StateMetrics {

    fn new() -> StateMetrics {
        StateMetrics {
            vessel_info: Family::default(),
            vessel_voyage: Family::default(),
            vessel_state: Family::default(),
            grid_position_in: Counter::default(),
            grid_position_out: Counter::default(),
        }
    }


    fn register(&self, registry: &mut Registry) {
        registry.register(
            "vessel_info",
            "Number vessel info updates",
            self.vessel_info.clone(),
        );
        registry.register(
            "vessel_voyage",
            "Number of vessel voyage updates",
            self.vessel_voyage.clone(),
        );
        registry.register(
            "vessel_state",
            "Number of vessel state updates (True: include, False: do not include)",
            self.vessel_state.clone(),
        );
        registry.register(
            "grid_position_in",
            "Number of vessel positions in the grid",
            self.grid_position_in.clone(),
        );
        registry.register(
            "grid_position_out",
            "Number of vessel positions out of the grid",
            self.grid_position_out.clone(),
        );
    }

}


#[derive(Clone, Debug, Hash, PartialEq, Eq, EncodeLabelSet)]
struct ItemLabel {
    operation: Operation
}


#[derive(Clone, Debug, Hash, PartialEq, Eq, EncodeLabelValue)]
enum Operation {
    _New,
    _Update,
    _Ignore,
}


#[derive(Clone, Debug, Hash, PartialEq, Eq, EncodeLabelSet)]
struct VesselStateLabel {
    in_region: i64,
    operation: Operation,
}
