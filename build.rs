use std::env;
use std::path::PathBuf;


fn main() {
    build_c();

    let libdir_path = PathBuf::from("src/lib/c")
        .canonicalize()
        .expect("valid canonical path");

    let headers_path = libdir_path.join("ais").join("decoder.h");
    let headers_path_str = headers_path.to_str().expect("Path is not a valid string");

    let bindings = bindgen::Builder::default()
        .header(headers_path_str)
        .blocklist_function("strtold")
        .blocklist_function("qecvt")
        .blocklist_function("qfcvt")
        .blocklist_function("qgcvt")
        .blocklist_function("ecvt_r")
        .blocklist_function("qecvt_r")
        .blocklist_function("qfcvt_r")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()))
        .generate()
        .expect("generated C code bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap()).join("bindings.rs");
    bindings
        .write_to_file(&out_path)
        .unwrap_or_else(|_| panic!("bindings should be written to file '{}'", out_path.to_str().unwrap_or("")));
}


fn build_c() {
    cc::Build::new()
        .file("./src/lib/c/ais/gpsd/bits.c")
        .file("./src/lib/c/ais/gpsd/driver_ais.c")
        .file("./src/lib/c/ais/decoder.c")
        .compile("gpsdbits");
}
