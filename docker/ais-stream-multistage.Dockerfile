FROM rust:1.79-slim-bookworm AS builder

RUN sed -i 's/^Components: main$/& contrib non-free/' /etc/apt/sources.list.d/debian.sources
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get install --assume-yes --no-install-recommends \
            pkg-config build-essential clang libgeos-dev libunrar-dev

WORKDIR /usr/src/ais-stream
COPY . .
RUN cargo install --path .

FROM debian:bookworm-slim AS ais-stream
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get install --assume-yes --no-install-recommends \
            libgeos*
COPY --from=builder /usr/local/cargo/bin/ais-stream /usr/local/bin/ais-stream
ENTRYPOINT ["ais-stream"]
