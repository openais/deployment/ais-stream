FROM debian:bookworm-slim AS ais-stream
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
        apt-get install --assume-yes --no-install-recommends \
            libgeos*
COPY ./target/release/ais-stream /usr/local/bin/ais-stream
ENTRYPOINT ["ais-stream"]
