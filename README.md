# AIS Stream

[TOC]

## About

AIS Stream is an application which allows connecting to an AIS TCP stream and broadcasting the received messages. AIS Stream is capable of emitting the received messages in raw (as received) or JSON format to various sinks (stdout, a file, a TCP repeater server or an HTTP SSE stream). For more info on how to use the application, see [Usage](#usage).

## Development

### Build requirements

- Rust bindgen: https://rust-lang.github.io/rust-bindgen/requirements.html

### Build dependencies

- build-essential
- clang
- libgeos-dev
- libunrar-dev
- pkg-config

#### Installing on Ubuntu

```bash
$ apt install build-essential clang libgeos-dev libunrar-dev pkg-config
```

## Usage

All examples below use the Norwegian Coastal Administration (`NCA`) AIS feed (`153.44.253.27:5631`) as an example.

### Forwarding raw AIS stream to stdout

Connects to the `NCA` AIS feed over TCP and writes the raw output to stdout:

```bash
ais-stream --format raw tcp://153.44.253.27:5631 -
```

### Forwarding decoded (JSON messages) AIS stream as HTTP SSE stream

Connects to the `NCA` AIS feed over TCP and starts and HTTP server which serves the messages transformed to JSON as a Server Sent Events (`SSE`) stream:

```bash
ais-stream --format decoded tcp://153.44.253.27:5631 http://localhost:9000
```

### Forwarding raw AIS stream to TCP

Connects to the `NCA` AIS feed over TCP and starts a new TCP server instance serving the afforementioned feed:

```bash
ais-stream --format raw tcp://153.44.253.27:5631 tcp://localhost:4100
```

### Transforming captured raw AIS feed to JSON AIS file

Capturing raw AIS feed and storing to file:

```bash
ais-stream --format raw tcp://153.44.253.27:5631 file:///abs/path/to/capture.nmea
```

Clean up the output file:

```bash
sed -i -E 's/OUTPUT: (.*)/\1/g' /abs/path/to/capture.nmea
```

Transform the captured stream:

```bash
ais-stream --format decoded file:///abs/path/to/capture.nmea file:///abs/path/to/json_capture.json
```

### Filtering a JSON AIS stream using jq

Filter on a specific ship:

```bash
ais-stream --format decoded tcp://153.44.253.27:5631 - | jq -R 'fromjson? | select(.shipname == "OKSHOLMEN" or .name == "OKSHOLMEN")'
```

Filter on vessels in the fishing category:

```bash
ais-stream --format decoded tcp://153.44.253.27:5631 - | jq -R 'fromjson? | select(.shiptype == 30)'
```

Filter on vessels in a specific region:

```bash
ais-stream --format decoded tcp://153.44.253.27:5631 - | jq -R 'fromjson? | select((.lon >= -4 and .lon <= 8) and (.lat >= 51 and .lat <= 57))'
```
